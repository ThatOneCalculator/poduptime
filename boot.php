<?php

/**
 * Boot the application, loading config and database connection.
 */

declare(strict_types=1);

use CodeZero\BrowserLocale\BrowserLocale;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use DetectLanguage\DetectLanguage;
use RedBeanPHP\R;

define('PODUPTIME', microtime(true));

require_once __DIR__ . '/vendor/autoload.php';

// Initialise language detection.
DetectLanguage::setApiKey($_SERVER['DETECTLANGUAGE_KEY']);
DetectLanguage::setSecure(true);

// Set up global DB connection.
R::setup(
    sprintf("pgsql:host=%s;port=%d;dbname=%s", $_SERVER['DB_HOST'], $_SERVER['DB_PORT'], $_SERVER['DB_DATABASE']),
    $_SERVER['DB_USERNAME'],
    $_SERVER['DB_PASSWORD'],
    true,
    true
);
R::testConnection() || die('Error in DB connection');

$browser = new BrowserLocale($_SERVER["HTTP_ACCEPT_LANGUAGE"] ?? 'en');
$locale = $browser->getLocale();
$t = new Translator($locale->language);
$t->addLoader('yaml', new YamlFileLoader());
$t->addResource('yaml', $_SERVER['BASE_DIR'] . '/translations/base.' . $locale->language . '.yaml', $locale->language);
$t->setFallbackLocales(['en']);
