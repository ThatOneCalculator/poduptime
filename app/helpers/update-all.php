<?php

/**
 * Pull pod info in parallel for all pods in database
 * Use php-cgi or set higher time limits in php.ini
 */

declare(strict_types=1);

set_time_limit(0);

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

use Carbon\Carbon;
use Poduptime\UpdateServerTask;
use Spatie\Async\Pool;

if (getMeta('pods_updating')) {
    die('already running');
}

if (!isConnected()) {
    die('no internet');
}

$time_start = microtime(true);

if (getMeta('pods_updating')) {
    die('already running');
}
addMeta('pods_updating', true);

$pods = allDomainsData() ?? [];

$pool = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);

foreach (array_column($pods, 'domain') as $domain) {
    $pool
        ->add(new UpdateServerTask($domain))
        ->catch(function ($exception) use ($domain) {
            podLog('error on updating ' . $exception, $domain, 'error');
        });
}

$pool->forceSynchronous()->wait();

addMeta('pods_updated');

if (Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('masterversions_updated', 'date_created'))->diffInHours() > ((int) $_SERVER['MASTER_GIT_REFRESH'] ?? 24)) {
    masterVersionCrawl() && addMeta('masterversions_updated');
}

$time_end = microtime(true);
$execution_time = ($time_end - $time_start) / 60;
addMeta('pods_update_runtime', round($execution_time));
addMeta('pods_updating', false);

$langhours  = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('languages_updated', 'date_created'))->diffInHours();

if ($langhours > ($_SERVER['LANGUAGE_REFRESH'] ?? 24)) {
    // @todo this should be per server not for the whole set of servers
    addMeta('languages_updated');
}
