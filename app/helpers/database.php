<?php

/**
 * backup database
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

//clean tables for dead servers - if below MIN_SCORE in .env we can delete their checks and clicks data from tables
cleanDatabase();
podLog('database cleanup done');
addMeta('database-clean');

//optimize database
optimiseDatabase();
podLog('databaase optimized');
addMeta('database-optimize');

//backup
backupData();
podLog('backup ran');
addMeta('backup');
