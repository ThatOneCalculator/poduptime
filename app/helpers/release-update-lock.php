<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

addMeta('pods_updating', false);
