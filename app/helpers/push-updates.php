<?php

/**
 * ping an API like friendica/mastodon
 */

declare(strict_types=1);

use RedBeanPHP\R;

require_once __DIR__ . '/../../boot.php';

try {
    $sql = 'SELECT domain FROM pods WHERE score = 0 and status = 0';
    $deadpods = R::getAll($sql);
} catch (\RedBeanPHP\RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $sql = "
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_users AS users,
            total_active_users_monthly AS monthly_users
        FROM monthlystats
        WHERE date_checked BETWEEN '2019-01-01' and now() AND softwarename = 'all'
        GROUP BY yymm, users, monthly_users
        ORDER BY yymm desc
        LIMIT 1
            ";
    $allusers = R::getAll($sql);
} catch (\RedBeanPHP\RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}


$servers = allServersList(null, true);
$count = number_format(floatval(allDomainsData(null, true)[0]['count']));
$ustot = number_format(floatval($allusers[0]['users']));
$montht = number_format(floatval($allusers[0]['monthly_users']));
$lastd = date("Y-m-d H:i", strtotime(getMeta('pods_updated', 'date_created')));
$dp = count($deadpods);
$bq = count($servers);

$string = "Found {$bq} new servers, {$dp} servers died off this update, [url=https://" . $_SERVER['DOMAIN'] . "/status]{$count} alive servers now at {$lastd} PT.[/url]\n\n{$ustot} Total Users, {$montht} Monthly Active Ssers. Check out the [url=https://" . $_SERVER['DOMAIN'] . "/stats]stats[/url]! \n\n";

if ($bq > 0) {
    $string .= "New #fediverse servers found:\n\n";
}

foreach ($servers as $server) {
    $string .= "[url=https://{$server['softwarename']}." . $_SERVER['DOMAIN'] . "/{$server['domain']}]{$server['domain']}[/url] a #{$server['softwarename']} server from {$server['countryname']}\n";
}

if ($dp > 0) {
    $string .= "\nDead servers: ";
}

foreach ($deadpods as $deadpod) {
    $string .= " [url=https://" . $_SERVER['DOMAIN'] . "/{$deadpod['domain']}]{$deadpod['domain']}[/url] ";
}

$string .= "\n\nHelp others find a home, send them to [url=https://" . $_SERVER['DOMAIN'] . "]" . $_SERVER['DOMAIN'] . "[/url]";

if ($bq > 0 || $dp > 0) {
    $conn = $_SERVER['PUSH_USER'] . ':' . $_SERVER['PUSH_PASS'] . ' ' . $_SERVER['PUSH_URL'];
    `curl -s -u $conn -d status="$string"`;
    podLog('Pushed updates', $string);
}
