<?php

/**
 * Compute monthly stats.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use RedBeanPHP\RedException as RedExceptionAlias;

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

try {
    $total_runs = R::getAll("
        SELECT
            count(value), 
            to_char(date_created, 'yyyy-mm') as yymm
        FROM meta
        WHERE name = 'pods_updated'
        GROUP BY yymm
    ");
} catch (RedExceptionAlias $e) {
    podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
}

try {
    // remove diasp.org heck in 2022 if you remove some older 2017 check data
    $monthly_totals_all = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            sum(total_users) as users,
            sum(active_users_halfyear) as active_users_halfyear,
            sum(active_users_monthly) as active_users_monthly,
            sum(local_posts) as posts,
            sum(comment_counts) as comments,
            count(domain) as pods,
            count(DISTINCT CASE WHEN domain = 'diasp.org' THEN id ELSE NULL END) as oldtotal,
            count(nullif(online, false)) as uptime, 
            count(nullif(online, true)) as downtime
        FROM checks
        GROUP BY yymm;
    ");
} catch (RedExceptionAlias $e) {
    podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
}

foreach ($monthly_totals_all as $monthly) {
    // Format date to timestamp.
    $timestamp = $monthly['yymm'] . '-01 01:01:01-01';

    $total = $monthly['oldtotal'];
    foreach ($total_runs as $runs) {
        if ($monthly['yymm'] === $runs['yymm']) {
            $total = $runs['count'];
            break;
        }
    }
    if ($total == 0) {
        $total = 1;
    }

    try {
        $p = R::findOrCreate('monthlystats', ['date_checked' => $timestamp, 'softwarename' => 'all']);
        $p['softwarename']                  = 'all';
        $p['total_users']                   = round($monthly['users'] / $total);
        $p['total_active_users_halfyear']   = round($monthly['active_users_halfyear'] / $total);
        $p['total_active_users_monthly']    = round($monthly['active_users_monthly'] / $total);
        $p['total_posts']                   = round($monthly['posts'] / $total);
        $p['total_comments']                = round($monthly['comments'] / $total);
        $p['total_pods']                    = round($monthly['pods'] / $total);
        if ($monthly['downtime']) {
            $p['total_uptime'] = round($monthly['downtime'] / $monthly['uptime'] * 100);
        } else {
            $p['total_uptime'] = 100;
        }

        R::store($p);
    } catch (RedExceptionAlias $e) {
        podLog('Error in SQL query at insert monthly for all' . $e->getMessage(), 'all', 'error');
    }
}

try {
    // remove diasp.org heck in 2022 if you remove some older 2017 check data
    $monthly_totals_bypod = R::getAll("
       SELECT
            to_char(checks.date_checked, 'yyyy-mm') AS yymm,
            sum(checks.total_users) as users,
            sum(checks.active_users_halfyear) as active_users_halfyear,
            sum(checks.active_users_monthly) as active_users_monthly,
            sum(checks.local_posts) as posts,
            sum(checks.comment_counts) as comments,
            count(checks.domain) as pods,
            count(DISTINCT CASE WHEN checks.domain = 'diasp.org' THEN checks.id ELSE NULL END) as oldtotal,
            count(nullif(checks.online, false)) as uptime, 
            count(nullif(checks.online, true)) as downtime, pods.softwarename as softwarename
        FROM checks
        INNER JOIN pods ON pods.domain = checks.domain GROUP BY yymm,pods.softwarename;
    ");
} catch (RedExceptionAlias $e) {
    podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
}

foreach ($monthly_totals_bypod as $monthly) {
    // Format date to timestamp.
    $timestamp = $monthly['yymm'] . '-01 01:01:01-01';

    $total = $monthly['oldtotal'];
    foreach ($total_runs as $runs) {
        if ($monthly['yymm'] === $runs['yymm']) {
            $total = $runs['count'];
            break;
        }
    }
    if ($total == 0) {
        $total = 1;
    }

    try {
        $p = R::findOrCreate('monthlystats', ['date_checked' => $timestamp, 'softwarename' => $monthly['softwarename']]);
        $p['softwarename']                = $monthly['softwarename'];
        $p['total_users']                 = $p['total_users']                  = round($monthly['users'] / $total);
        $p['total_active_users_halfyear'] = $p['total_active_users_halfyear']  = round($monthly['active_users_halfyear'] / $total);
        $p['total_active_users_monthly']  = $p['total_active_users_monthly']   = round($monthly['active_users_monthly'] / $total);
        $p['total_posts']                 = $p['total_posts']                  = round($monthly['posts'] / $total);
        $p['total_comments']              = round($monthly['comments'] / $total);
        $p['total_pods']                  = round($monthly['pods'] / $total);
        if ($monthly['downtime'] && $monthly['uptime']) {
            $p['total_uptime'] = round($monthly['downtime'] / $monthly['uptime'] * 100);
        } elseif ($monthly['downtime']) {
            $p['total_uptime'] = round($monthly['downtime'] / 1 * 100);
        } else {
            $p['total_uptime'] = 100;
        }

        R::store($p);
    } catch (RedExceptionAlias $e) {
        podLog('Error in SQL query at insert monthly for a software' . $e->getMessage(), $monthly['softwarename'], 'error');
    }
}

podLog('monthly stats updated');
addMeta('statstable_updated');
