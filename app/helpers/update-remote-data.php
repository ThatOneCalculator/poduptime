<?php

/**
 * Crawl and add all pods from other monitoring sites to the list.
 */

declare(strict_types=1);

set_time_limit(0);

use Poduptime\AddServerTask;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Pool;

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

$existingPods = allDomainsData();

// get pods we have not tried to auto add for a while
try {
    $triedPods = R::getCol("
        SELECT value
        FROM meta
        WHERE date_created > now() - interval '48 hours'
        AND name = 'add_attempt'
     ");
} catch (RedException $e) {
    podLog('Error in SQL query: ' . $e->getMessage());
}

$skipPods = array_merge($existingPods->domain ?? [], $triedPods ?? []);

$fetchedPods = collect(c('pod-list-providers'))
    ->flatMap(fn($podListProvider) => extractPodsFromCurl(
        $podListProvider['curl'](),
        $podListProvider['field'] ?? null,
        $podListProvider['column'] ?? null
    ))
    ->unique();

$processPods       = $fetchedPods->diff($skipPods);

$remotepool        = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);

foreach ($processPods as $domain) {
    if (is_string($domain)) {
        cleanDomain($domain);
        $remotepool
            ->add(new AddServerTask($domain))
            ->catch(function ($exception) use ($domain) {
                $_SERVER['APP_DEBUG'] && podLog('Failed remote add ' . $exception, $domain, 'warning');
            });
        addMeta('add_attempt', $domain);
    }
}

$remotepool->forceSynchronous()->wait();

podLog('federation crawl done');
addMeta('federation_updated');
