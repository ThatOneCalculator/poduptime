<?php

/**
 * Redirect to a given pod or find a good fit.
 */

declare(strict_types=1);

use Carbon\Carbon;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use RedBeanPHP\R;
use Poduptime\PodStatus;
use RedBeanPHP\RedException;
use RedBeanPHP\RedException\SQL;

// Other parameters.
$_domain   = $_GET['domain'] ?? '';
$_software = $_GET['software'] ?? '';

require_once __DIR__ . '/../../boot.php';

$country_code = ipLocation($_SERVER['REMOTE_ADDR'])['country'];

$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

try {
    $sql = 'SELECT domain
            FROM pods
            WHERE signup
                AND uptime_alltime > 99
                AND monthsmonitored > 2
                AND status = ?
                AND softwarename NOT SIMILAR TO ?
                AND domain NOT SIMILAR TO ?
            ';
    $click  = 'autoclick';
    if ($_domain) {
        $click  = 'manualclick';
        $domain = R::getCell('SELECT domain FROM pods WHERE domain LIKE ?', [$_domain]);
    } elseif ($country_code && $_software) {
        $sql   .= ' AND softwarename = ? AND country = ? ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains, $_software, $country_code]);
    } elseif ($_software) {
        $sql   .= ' AND softwarename = ? ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains, $_software]);
    } elseif ($country_code) {
        $sql   .= ' AND country = ? ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains, $country_code]);
    } else {
        $sql   .= ' ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains]);
    }
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

if ($domain) {
    $c           = R::dispense('clicks');
    $c['domain'] = $domain;
    $c[$click]   = 1;
    if (!(new CrawlerDetect())->isCrawler()) {
        try {
            R::store($c);
        } catch (SQL $e) {
        }
    }
    podLog('go', $domain);
    setcookie('domain', $domain, Carbon::now()->addCentury()->timestamp);
    header('X-Robots-Tag: nofollow');
    header('Location: https://' . $domain);
} else {
    echo '<div class="text-lg-center fw-bold">No server found that meets your requirements, please keep looking for a server.</div>';
    include_once __DIR__ . '/index.php';
}
