<?php

/**
 * Edit an existing pod.
 */

declare(strict_types=1);

use Carbon\Carbon;
use Poduptime\PodStatus;
use RedBeanPHP\R;
use RedBeanPHP\RedException;

// Required parameters.
($_domain = htmlspecialchars($_GET['domain']) ?? null) || die($t->trans('admin.noserver'));
($_token = $_GET['token'] ?? null) || die($t->trans('admin.notoken'));
strlen($_token) > 6 || die($t->trans('admin.badtoken'));

// Other parameters.
$_action                 = $_GET['action'] ?? '';
$_weight                 = $_GET['weight'] ?? 10;
$_email                  = $_GET['email'] ?? '';
$_terms                  = $_GET['terms'] ?? '';
$_pp                     = $_GET['pp'] ?? '';
$_support                = $_GET['support'] ?? '';
$_podmin_statement       = $_GET['podmin_statement'] ?? '';
$_podmin_notify          = $_GET['podmin_notify'] ?? 0;
$_podmin_notify_level    = $_GET['podmin_notify_level'] ?? 50;

require_once __DIR__ . '/../../boot.php';

try {
    $pod = R::findOne('pods', 'domain = ?', [$_domain]);
    $pod || die($t->trans('admin.domainnotfound'));
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

$pod['token'] === $_token || die($t->trans('admin.mismatch'));
$pod['tokenexpire'] >= date('Y-m-d H:i:s') || die($t->trans('admin.expired'));

// Delete and exit.
if ('delete' === $_action) {
    try {
        $pod['status'] = PodStatus::USER_DELETED;
        R::store($pod);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    podLog('server deleted by owner', $_domain);
    die($t->trans('admin.deleted'));
}

// Pause and exit.
if ('pause' === $_action) {
    try {
        $pod['status'] = PodStatus::PAUSED;
        R::store($pod);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    podLog('server paused by owner', $_domain);
    die($t->trans('admin.paused'));
}

// Un-Pause and exit.
if ('unpause' === $_action) {
    try {
        $pod['status'] = PodStatus::RECHECK;
        R::store($pod);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    podLog('server unpaused by owner', $_domain);
    die($t->trans('admin.unpaused'));
}

// Save and exit.
if ('save' === $_action) {
    $_weight <= 10 || die($t->trans('admin.outofrange'));

    try {
        $pod['email']                  = $_email;
        $pod['weight']                 = $_weight;
        $pod['terms']                  = $_terms;
        $pod['pp']                     = $_pp;
        $pod['support']                = $_support;
        $pod['podmin_statement']       = strip_tags($_podmin_statement);
        $pod['podmin_notify']          = $_podmin_notify;
        $pod['podmin_notify_level']    = $_podmin_notify_level;

        R::store($pod);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    sendEmail($_email, $t->trans('admin.editemailsubject'), $t->trans('admin.editemailbody', ['%(domain)' => $_domain]));
    podLog('server updated by owner', $_domain);
    die($t->trans('admin.editsuccess'));
}

?>
<script>
    window.onload = function() {
        $("#weight").ionRangeSlider({
            skin: "big"
        });
        $("#podmin_notify_level").ionRangeSlider({
            skin: "big"
        });
    }
</script>
<b><?php echo $t->trans('admin.authorized', ['%(domain)' => $_domain, '%(hours)' => (new Carbon($pod['tokenexpire']))->locale($locale->language)->diffForHumans(null, true)]) ?></b>
<form action="/">
    <input type="hidden" name="edit">
    <input type="hidden" name="domain" value="<?php echo $_domain; ?>">
    <input type="hidden" name="token" value="<?php echo $_token; ?>">
    <label><?php echo $t->trans('admin.emailrequired') ?> <input type="text" name="email" value="<?php echo $pod['email']; ?>"></label><br>
    <label><?php echo $t->trans('admin.termsurl') ?> <input type="text" name="terms" value="<?php echo $pod['terms']; ?>"></label><br>
    <label><?php echo $t->trans('admin.ppurl') ?> <input type="text" name="pp" value="<?php echo $pod['pp']; ?>"></label><br>
    <label><?php echo $t->trans('admin.supporturl') ?> <input type="text" cc name="support" value="<?php echo $pod['support']; ?>"></label><br>
    <label><?php echo $t->trans('admin.statement') ?> <br><textarea cols="150" rows="10" class="col-10" maxlength="500" name="podmin_statement"><?php echo $pod['podmin_statement']; ?></textarea></label><br>
    <label><?php echo $t->trans('admin.weight') ?> <input type="text" class="col-2" data-min="0" data-step="1" data-max="10" name="weight" id="weight" data-from="<?php echo $pod['weight']; ?>"><?php echo $t->trans('admin.weightnote') ?></label><br>
    <label><?php echo $t->trans('admin.notify') ?> <input type="checkbox" name="podmin_notify" <?php echo $pod['podmin_notify'] ? 'checked' : ''; ?> ></label><br>
    <label><?php echo $t->trans('admin.notifylevel') ?> <input type="text" data-min="1" data-step="1" data-max="99" name="podmin_notify_level" id="podmin_notify_level" data-from="<?php echo $pod['podmin_notify_level']; ?>"></label><br>

    <button class="btn-success btn" type="submit" name="action" value="save"><?php echo $t->trans('base.general.save') ?></button>
</form>
<br>
<br><?php echo $t->trans('admin.status') ?>: <?php echo PodStatus::getKey((int) $pod['status']); ?>
<br>
<form action="/">
    <input type="hidden" name="edit">
    <input type="hidden" name="domain" value="<?php echo $_domain; ?>">
    <input type="hidden" name="token" value="<?php echo $_token; ?>">
    <button class="btn btn-danger" type="submit" name="action" value="delete" aria-describedby="delete"><?php echo $t->trans('base.general.delete') ?></button>
</form>
<small id="delete" class="form-text text-muted">
    <?php echo $t->trans('admin.delete') ?>
</small>
<form action="/">
    <input type="hidden" name="edit">
    <input type="hidden" name="domain" value="<?php echo $_domain; ?>">
    <input type="hidden" name="token" value="<?php echo $_token; ?>">
    <button class="btn btn-warning" type="submit" name="action" value="pause" aria-describedby="pause"><?php echo $t->trans('base.general.pause') ?></button>
</form>
<small id="pause" class="form-text text-muted">
    <?php echo $t->trans('admin.pause') ?>
</small>
<form action="/">
    <input type="hidden" name="edit">
    <input type="hidden" name="domain" value="<?php echo $_domain; ?>">
    <input type="hidden" name="token" value="<?php echo $_token; ?>">
    <button class="btn btn-warning" type="submit" name="action" value="unpause" aria-describedby="undelete"><?php echo $t->trans('base.general.unpause') ?></button>
</form>
<small id="undelete" class="form-text text-muted">
    <?php echo $t->trans('admin.unpause') ?>
</small>
<br>
<?php echo $t->trans('admin.conerrors') ?>:
<?php
try {
    $errors = R::getAll('
            SELECT error, date_checked
            FROM checks
            WHERE domain = ?
            AND ONLINE = false
            ORDER BY date_checked DESC
            LIMIT 20
        ', [$_domain]);
} catch (RedException $e) {
    podLog('Error in SQL query' . $e->getMessage());
}

?>

<table class="table col-lg-8">
    <thead class="thead-light">
    <tr>
        <th scope="col" class="col-lg-9"><?php echo $t->trans('base.general.error') ?></th>
        <th scope="col" class="col-md-3"><?php echo $t->trans('base.general.date') ?></th>
    </tr>
    </thead>
    <tbody>

<?php
foreach ($errors as $error) {
    echo "<tr><td>{$error['error']}</td><td>{$error['date_checked']}</td></tr>";
}
?>

    </tbody>
</table>
