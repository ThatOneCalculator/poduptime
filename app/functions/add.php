<?php

/**
 * Add a new pod.
 */

declare(strict_types=1);

use Laminas\Validator\Hostname;
use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

$_domain              = cleanDomain($_GET['domain']) ?? null || die($t->trans('admin.noserver'));

$_email               = $_GET['email'] ?? '';

if (checkNodeinfo($_domain)) {
    $pod = R::findOne('pods', 'domain = ?', [$_domain]);

    if ($pod['email']) {
        echo $t->trans('admin.dupeserver');
        return;
    }

    if ($pod) {
        $key = $pod['publickey'];
        $ip_data = ipData($pod['domain']);
        $txts = str_replace('"', "", (array) $ip_data['txt']) ?? [];
        if (!in_array($key, $txts, true)) {
            echo $t->trans('admin.dnsrecord') . '<br><b>';
            echo $_domain . ' IN TXT "' . $pod['publickey'] . '"';
            echo '</b><br>';
            echo $t->trans('admin.dnsnote');
            return;
        } elseif (in_array($key, $txts, true) && $_email) {
            try {
                $pod['email'] = $_email;
                R::store($pod);
            } catch (RedException $e) {
                die('Error in SQL query: ' . $e->getMessage());
            }
            echo $t->trans('admin.emailsuccess') . '<br>';
        } elseif (!$_email) {
            echo $t->trans('admin.emailmissing') . '<br>';
        }
    }

    try {
        $publickey = $_SERVER['DOMAIN'] . "-site-verification=" . bin2hex(random_bytes(9));
    } catch (Exception $e) {
    }
    try {
        $p = R::dispense('pods');
        $p['domain'] = $_domain;
        $p['email'] = $_email;
        $p['publickey'] = $publickey;
        $p['date_updated'] = date('Y-m-d H:i:s');
        $success = R::store($p);
    } catch (RedException $e) {
        Podlog('Error in SQL query' . $e->getMessage(), '$_domain', 'error');
    }

    $_email && sendEmail($_email, $t->trans('admin.addemailsubject'), $t->trans('admin.addemailbody', ['%(domain)' => $_domain]), $_SERVER['ADMIN_EMAIL']);
    if ($success) {
        echo $t->trans('admin.addsuccess');
    }
} else {
    echo $t->trans('admin.error', ['%(domain)' => $_domain]);
}
