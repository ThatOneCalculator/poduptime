    $(document).ready(function () {
        var domain = $('input:input[name="domain"]').val();
        $('#jsonrawclick').on('click', function () {
            $.ajax({
                type: 'POST',
                url: /api/,
                async: true,
                data: {"query":"{node(domain: \"" + domain + "\"){id name metatitle metadescription detectedlanguage metaimage owner onion i2p ip ipv6 dnssec sslexpire servertype camo terms pp support softwarename shortversion fullversion masterversion daysmonitored monthsmonitored date_updated date_laststats date_created metalocation country city state zipcode countryname lat long uptime_alltime latency sslexpire total_users active_users_monthly active_users_halfyear score status signup podmin_statement}}"},
                dataType: 'json',
                success: function (json) {
                    var jsonPretty = JSON.stringify(json.data.node, null, ' ');
                    $('#jsonraw').empty().append(jsonPretty);
                },
                error: function (e) {
                    alert(e.message);
                }
            });
        });
        $('#jsonerrorclick').on('click', function () {
            $.ajax({
                type: 'POST',
                url: /api/,
                async: true,
                data: {"query":"{checks(online: \"false\", domain: \"" + domain + "\", limit: 25){date_checked error online}}"},
                dataType: 'json',
                success: function (json) {
                    var jsonPretty = JSON.stringify(json.data.checks, null, ' ');
                    $('#jsonerror').empty().append(jsonPretty);
                },
                error: function (e) {
                    alert(e.message);
                }
            });
        });
});

