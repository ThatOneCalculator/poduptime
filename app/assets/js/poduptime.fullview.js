var $table = $('.tablesorter'),
    pagerOptions = {
        container: $(".pager"),
        output: '{startRow} - {endRow}',
        removeRows: false,
        size: 15,
        savePages : true,
        cssPageSize: '.pagesize'
    };
    $table
    .tablesorter({
        theme: 'bootstrap',
        headerTemplate: '{content} {icon}',
        widthFixed: true,
        widgets: ['columnSelector', 'zebra', 'columns', 'filter', 'saveSort'],
        widgetOptions: {
            columnSelector_container: $('#columnSelector'),
            columnSelector_saveColumns: true,
            columnSelector_layout: '<label><input type="checkbox">{name}</label>',
            columnSelector_layoutCustomizer: null,
            columnSelector_name: 'data-selector-name',
            columnSelector_mediaquery: true,
            columnSelector_mediaqueryName: 'Auto ',
            columnSelector_mediaqueryState: true,
            columnSelector_mediaqueryHidden: true,
            columnSelector_breakpoints: ['20em', '30em', '40em', '50em', '60em', '70em']
        }
    })
    .tablesorterPager(pagerOptions);

    $(document).ready(function () {
        $('.columnssimple').on('click', function () {
            $('table').trigger('refreshColumnSelector', true);
            $('#colSelect1').prop('checked', false);
            $('.columnSelectorWrapper').hide();
            $('.columnssimple').hide();
            $('.resetfilters').hide();
            $('.pagesize').hide();
            $('.columnsadvanced').show().css('display', 'inline-block');
            $('table').trigger('filterReset');
        });
        $('.columnsadvanced').on('click', function () {
            $('table').trigger('refreshColumnSelector', ['columns', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]]);
            $('#colSelect1').prop('checked', false);
            $('.columnSelectorWrapper').show().css('display', 'inline-block');
            $('.columnssimple').show().css('display', 'inline-block');
            $('.resetfilters').show().css('display', 'inline-block');
            $('.pagesize').show().css('display', 'inline-block');
            $('.columnsadvanced').hide();
        });
        $('.resetfilters').on('click', function () {
            $('table').trigger('filterReset');
        });
        var software = $('input:input[name="software"]').val();
        $.get('app/views/tabledata.php?software=' + software, function (html) {
            $('table tbody').append(html);
            $('table').trigger('update', [true]);
            $('.loadingtable').slideToggle('slow');
        })
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        $("#myModal").on("show.bs.modal", function (event) {
            $(this).find(".modal-body").html('');
            var button = $(event.relatedTarget)
            var title = button.data('title')
            var url = button.data('url')
            var message = button.data('message')
            if (url) {
                $(this).find(".modal-body").load(url);
            }
            if (message) {
                $(this).find(".modal-body").html(message);
            }
            $(this).find(".modal-title").html(title);
        });

    });

