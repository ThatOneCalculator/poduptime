<?php

/**
 * Include JS for stats view.
 */

declare(strict_types=1);

use Poduptime\PodStatus;
use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

try {
    $totals = R::getAll('
        SELECT
            softwarename,
            count(*) AS pods,
            sum(total_users) AS users,
            round(avg(uptime_alltime),2) AS uptime
        FROM pods
        WHERE status < ?
        AND score > 0
        GROUP BY softwarename
        ORDER BY users desc, softwarename limit 10
    ', [PodStatus::PAUSED]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $totalsbycountry = R::getAll('
        SELECT
            count(*) AS pods,
            countryname
        FROM pods
        WHERE status < ?
        AND score > 0
        AND countryname IS NOT NULL
        GROUP BY countryname
        ORDER BY pods desc, countryname limit 10
    ', [PodStatus::PAUSED]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_users = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_users AS users
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN '2019-01-01' and now()
        GROUP BY yymm, users
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_users_active = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_active_users_halfyear AS active_users_halfyear,
            total_active_users_monthly AS active_users_monthly
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN '2019-01-01' and now()
        GROUP BY yymm, active_users_halfyear, active_users_monthly
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_pods = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_pods AS pods
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN '2019-01-01' and now()
        GROUP BY yymm, pods
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_comments = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_comments AS comments
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN '2019-01-01' and now()
        GROUP BY yymm, comments
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_posts = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_posts AS posts
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN '2019-01-01' and now()
        GROUP BY yymm, posts
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
?>
<script>
    //import { Chart } from 'chart.js/auto';

    /**
     * Add a new pie chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    function addPieChart(id, data) {
        var intToRGB = function(value, alpha, max) {
            var valueAsPercentageOfMax = value / max;
            // actual max is 16777215 but represnts white so we will take a max that is
            // below this to avoid white
            var MAX_RGB_INT = 16600000;
            var valueFromMaxRgbInt = Math.floor(MAX_RGB_INT * valueAsPercentageOfMax);
            //credit to https://stackoverflow.com/a/2262117/2737978 for the idea of how to implement
            var blue = Math.floor(valueFromMaxRgbInt % 256);
            var green = Math.floor(valueFromMaxRgbInt / 256 % 256);
            var red = Math.floor(valueFromMaxRgbInt / 256 / 256 % 256);
            return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
        }
        var MAX = data.length;
        var backgroundColors = data.map(function(item) {
            return intToRGB(item, 0.8, MAX);
        });
        var borderColors = data.map(function(item) {
            return intToRGB(item, 1, MAX);
        });
        new Chart(document.getElementById(id), {
            type: "pie",
            data: {
                labels: <?php echo json_encode(array_column($totals, 'softwarename')); ?>,
                datasets: [{
                    data: data,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                legend: {
                    display: false,
                    labels: {
                        fontSize: 9,
                        boxWidth: 4
                    }
                }
                }
            }
        });
    }
    function addPieChartCountry(id, data) {
        var intToRGB = function(value, alpha, max) {
            var valueAsPercentageOfMax = value / max;
            // actual max is 16777215 but represnts white so we will take a max that is
            // below this to avoid white
            var MAX_RGB_INT = 16600000;
            var valueFromMaxRgbInt = Math.floor(MAX_RGB_INT * valueAsPercentageOfMax);
            //credit to https://stackoverflow.com/a/2262117/2737978 for the idea of how to implement
            var blue = Math.floor(valueFromMaxRgbInt % 256);
            var green = Math.floor(valueFromMaxRgbInt / 256 % 256);
            var red = Math.floor(valueFromMaxRgbInt / 256 / 256 % 256);
            return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
        }
        var MAX = data.length;
        var backgroundColors = data.map(function(item) {
            return intToRGB(item, 0.8, MAX);
        });
        var borderColors = data.map(function(item) {
            return intToRGB(item, 1, MAX);
        });
        new Chart(document.getElementById(id), {
            type: "pie",
            data: {
                labels: <?php echo json_encode(array_column($totalsbycountry, 'countryname')); ?>,
                datasets: [{
                    data: data,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16,
                            boxWidth: 4
                        }
                    }
                }
            }
        });
    }
    /**
     * Add a new line chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    function addLineChartusers(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_users, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: 'Users',
                    fill: false,
                    borderColor: "#304C89",
                    backgroundColor: "#304C89",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                }
            }
        });
    }
    function addLineChartusersactive(id, data1, data2) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_users_active, 'yymm')); ?>,
                datasets: [{
                    data: data1,
                    label: 'Active Users Halfyear',
                    fill: false,
                    borderColor: "#648DE5",
                    backgroundColor: "#648DE5",
                    borderWidth: 3,
                    pointHoverRadius: 6
                },
                    {
                        data: data2,
                        label: 'Active Users Monthly',
                        fill: false,
                        borderColor: "#548687",
                        backgroundColor: "#548687",
                        borderWidth: 3,
                        pointHoverRadius: 6
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
            }
        });
    }
    function addLineChartpods(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_pods, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: 'Servers',
                    fill: false,
                    borderColor: "#B0413E",
                    backgroundColor: "#B0413E",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
            }
        });
    }
    function addLineChartcomments(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_comments, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: 'Comments',
                    fill: false,
                    borderColor: "#CDC392",
                    backgroundColor: "#CDC392",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
            }
        });
    }
    function addLineChartposts(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_posts, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: 'Posts',
                    fill: false,
                    borderColor: "#2B547E",
                    backgroundColor: "#2B547E",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
            }
        });
    }
    addPieChart('total_network_users', <?php echo json_encode(array_column($totals, 'users')); ?>);
    addPieChart('total_network_pods', <?php echo json_encode(array_column($totals, 'pods')); ?>);
    addPieChartCountry('total_servers_country', <?php echo json_encode(array_column($totalsbycountry, 'pods')); ?>);
    addLineChartusers('user_growth', <?php echo json_encode(array_column($check_total_users, 'users')); ?>);
    addLineChartusersactive('user_growth_active', <?php echo json_encode(array_column($check_total_users_active, 'active_users_halfyear')); ?>, <?php echo json_encode(array_column($check_total_users_active, 'active_users_monthly')); ?>);
    addLineChartpods('pod_growth', <?php echo json_encode(array_column($check_total_pods, 'pods')); ?>);
    addLineChartcomments('comment_growth', <?php echo json_encode(array_column($check_total_comments, 'comments')); ?>);
    addLineChartposts('posts_growth', <?php echo json_encode(array_column($check_total_posts, 'posts')); ?>);
</script>
