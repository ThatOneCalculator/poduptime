<?php

/**
 * get data for showfull.php calls.
 */

$seconds_to_cache = 3600;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");

use Carbon\Carbon;
use Poduptime\PodStatus;

require_once __DIR__ . '/../../boot.php';

$iso = new Matriphe\ISO639\ISO639();
$country_code = ipLocation($_SERVER['REMOTE_ADDR'])['country'];
$pods = allServersList($_GET['software']);

foreach ($pods as $pod) {
    $humanmonitored = Carbon::now()->subDays($pod['daysmonitored'])->diffForHumans(null, true);
    $last_podcheck  = Carbon::createFromFormat('Y-m-d H:i:s', $pod['date_laststats'])->diffForHumans(null, true);
    echo '<tr><td data-placement="bottom" data-toggle="tooltip"><a class="url" target="_pod" href="/go&domain=' . $pod['domain'] . '">' . idn_to_utf8($pod['domain']) . '</a><a href="/' . $pod['domain']  . '"><span class="icon1 fa fa-info-circle align-top ps-3"></span></a></td>';
    if ($pod['shortversion'] > $pod['masterversion']) {
        $version = $pod['shortversion'];
    } elseif (!$pod['shortversion']) {
        $version = '';
    } else {
        $version = $pod['shortversion'];
    }
    $classver = 'green';
    if (version_compare($pod['shortversion'] ?? '', $pod['masterversion'] ?? '', '=')) {
        $classver = 'black';
    } elseif (version_compare($pod['shortversion'] ?? '', $pod['masterversion'] ?? '', '<')) {
        $classver = 'text-danger';
    }
    echo '<td>' . $pod['name'] . '</td>';
    echo '<td class="' . $classver . '">' . $version . '</td>';
    echo '<td>' . $pod['softwarename'] . '</td>';
    echo '<td><a class="norightclick" data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-url="app/views/podstat-uptime.php?domain=' . $pod['domain'] . '">' . ($pod['uptime_alltime'] > 0 ? $pod['uptime_alltime'] . '%' : '') . '</a></td>';
    echo '<td>' . ($pod['latency'] > 0 ? $pod['latency'] : '') . '</td>';
    echo '<td>' . ($pod['signup'] ? $t->trans('base.general.yes') : $t->trans('base.general.no')) . '</td>';
    echo '<td><a class="norightclick" data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-url="app/views/podstat-user-counts.php?domain=' . $pod['domain'] . '">' . ($pod['total_users'] > 0 ? $pod['total_users'] : '') . '</a></td>';
    echo '<td>' . ($pod['active_users_halfyear'] > 0 ? $pod['active_users_halfyear'] : '') . '</td>';
    echo '<td>' . ($pod['active_users_monthly'] > 0 ? $pod['active_users_monthly'] : '') . '</td>';
    echo '<td><a class="norightclick" data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-url="app/views/podstat-actions-counts.php?domain=' . $pod['domain'] . '"> ' . ($pod['local_posts'] > 0 ? $pod['local_posts'] : '') . '</a></td>';
    echo '<td>' . ($pod['comment_counts'] > 0 ? $pod['comment_counts'] : '') . '</td>';
    echo '<td><div data-toggle="tooltip">' . $pod['monthsmonitored'] . '</div></td>';
    echo '<td><div>' . $pod['score'] . '</div></td>';
    echo '<td><div>' . ($pod['status'] ? $t->trans('base.general.up') : $t->trans('base.general.down')) . '</div></td>';
    if ($country_code === $pod['country']) {
        echo '<td class="text-success" data-toggle="tooltip" title="Country: ' . ($pod['countryname'] ?? '') . '"><b>' . $pod['country'] . '</b></td>';
    } else {
        echo '<td data-toggle="tooltip" title="Country: ' . ($pod['countryname'] ?? 'n/a') . '">' . $pod['country'] . '</td>';
    }
    echo '<td>' . $pod['city'] . '</td>';
    echo '<td>' . $pod['state'] . '</td>';
    echo '<td data-toggle="tooltip" title="' . ($pod['detectedlanguage'] ? $iso->languageByCode1($pod['detectedlanguage']) : '') . '">' . ($pod['detectedlanguage'] ? strtoupper($pod['detectedlanguage']) : '') . '</td>';
}
?>
<script>
$(document).ready(function () {
$(".norightclick").on("contextmenu", function () {
            return false;
        });
});
</script>
