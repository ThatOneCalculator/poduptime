<?php

require_once __DIR__ . '/../../boot.php';

$software = !empty($subdomain) ? $subdomain : '';

if (!empty($subdomain)) {
    $smallwrapper = 'smallnone';
    $bigwrapper = 'bigwrapper';
    $hidden = 'hidden';
} else {
    $smallwrapper = 'smallwrapper';
    $bigwrapper = 'bignone';
    $hidden = '';
}

?>

<div class="container">
    <div class="d-flex justify-content-center p-3"><h1><b class="fw-bold text-success"><?php echo $t->trans('welcome.main.find') ?></b></h1></div>
    <div class="d-flex justify-content-center p-3"><h4><strong><?php echo $t->trans('welcome.main.suggested') ?></strong></h4></div>
    <div>
        <?php
        $closestservers = closestServers($_SERVER['REMOTEADDR'] ?? $_SERVER['REMOTE_ADDR'], 6, !empty($subdomain) ? $subdomain : '%');
        if (count($closestservers) > 0) {
            foreach ($closestservers as $server) {
                $location = $server['city'] ?? $server['state'] ?? $server['countryname'];
                echo '<div class="d-flex justify-content-md-center"><a class="fw-bold text-success pb-1" href="/' . $server['domain'] . '">';
                echo $t->trans('welcome.main.picked', ['%(domain)' => $server['domain'], '%(software)' => $server['softwarename'], '%(location)' => $location]);
                echo '</a></div>';
            }
        } else {
            $location = ipLocation($_SERVER['REMOTEADDR'] ?? $_SERVER['REMOTE_ADDR']);
            echo '<div class="d-flex justify-content-md-center fw-bold text-danger">';
            echo $t->trans('welcome.main.nopicked', ['%(software)' => !empty($subdomain) ? $subdomain : '%', '%(location)' => $location]);
            echo '</div>';
        }
        ?>
    </div>
    <div class="d-flex justify-content-center pt-3"><h4><?php echo $t->trans('welcome.main.keeplooking') ?></h4></div>
    <div class="d-flex justify-content-center pb-3"><h3><?php echo $t->trans('welcome.main.first') ?></h3></div>
            <div class="row row-cols-auto justify-content-center">
                <?php
                $softwares = c('softwares');
                foreach ($softwares as $soft => $welcome_item) {
                    if (isset($welcome_item['welcome']) && $welcome_item['welcome'] === '1') {
                        if ($welcome_item['text'] == $software) {
                            $iconselector = 'iconselected';
                        } else {
                            $iconselector = 'iconnotselected';
                        }
                        printf(
                            '<div class="p-2 %5$s %9$s"><span class="fa-ani"><a href="//%2$s.%6$s%7$s"><i data-toggle="tooltip" data-placement="right" title="%10$s" class="fa %4$s fa-2x" aria-hidden="true"></i></a><div class="text-muted %8$s">%3$s</div></span></div>',
                            '',
                            $welcome_item['href'],
                            $welcome_item['text'],
                            $welcome_item['faclass'],
                            $smallwrapper,
                            $_SERVER['DOMAIN'],
                            $_SERVER['REQUEST_URI'],
                            $hidden,
                            $iconselector,
                            $t->trans('softwares.' . $welcome_item['text'])
                        );
                    }
                }
                ?>
            </div>
<br>
    <div class="d-flex justify-content-center p-2"><h3><?php echo $t->trans('welcome.main.second') ?></h3></div>
        <div class="d-lg-flex justify-content-center">
            <div class="<?php echo $bigwrapper ?> p-4">
            <span class="col-sm-auto fa-ani">
                <a href="/map"><i class="fa fa-map-marker fa-2x" title="Map" aria-hidden="false"></i></a>
            </span>
                <br><?php echo $t->trans('welcome.main.map') ?>
            </div>
            <div class="<?php echo $bigwrapper ?> p-4">
            <span class="col-sm-auto fa-ani">
                <a href="/list"><i class="fa fa-list-alt fa-2x" title="List" aria-hidden="false"></i></a>
            </span>
                <br><?php echo $t->trans('welcome.main.list') ?>
        </div>
    </div>
</div>
<div class="pb-lg-5 mb-5"></div>
