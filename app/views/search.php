<?php

/**
 * get data and display for search query.
 */

declare(strict_types=1);

use Poduptime\PodStatus;
use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

$iso = new Matriphe\ISO639\ISO639();

$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

$urlArr = parse_url($_SERVER['REQUEST_URI']);
if (!isset($urlArr['query'])) {
    echo 'No search query<br>';
} else {
    parse_str($urlArr['query'], $output);
    $query = strip_tags(utf8_encode($output['query']));
}

if ($query) {
    try {
        $pods = R::getAll('
        SELECT domain, softwarename, signup, name, detectedlanguage, status, metatitle, metadescription, weightedscore, metalocation, zipcode, countryname
        FROM pods
        WHERE status = :PodStatus
          AND softwarename NOT SIMILAR TO :hiddensoftwares
          AND domain NOT SIMILAR TO :hiddendomains
          AND (
              (lower(domain) LIKE :query)
              OR (lower(metatitle) LIKE :query)
              OR (lower(metadescription) LIKE :query)
              OR (lower(metalocation) LIKE :query)
              OR (lower(zipcode) LIKE :zipquery)
              )
        ORDER BY weightedscore DESC
    ', [':PodStatus' => PodStatus::UP, ':zipquery' => substr(strtolower($query), 0, 2) . '%', ':query' => '%' . strtolower($query) . '%', ':hiddensoftwares' => $hiddensoftwares, ':hiddendomains' => $hiddendomains]);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

if ($pods) {
    echo '<div class="container-fluid">';
    echo '<div class="col m-lg-2 pb-2">' . $t->trans('base.strings.search.results', ['%(number)' => count($pods), '%(searchterm)' => $query]) . '</div>';
    foreach ($pods as $pod) {
        echo '<div class="row ps-1 ms-2">';
        echo '<div class="col small"><a class="" target="_pod" href="/go&domain=' . $pod['domain'] . '">https://' . idn_to_utf8($pod['domain']) . '</a><a class="d-inline-block small ps-2 mb-1" href="/' . $pod['domain'] . '"><span class="d-inline-block fa fa-line-chart"></span></a></div>';
        echo '</div><div class="row ps-1 ms-2">';
        echo '<div class="col text-primary h4"><a class="" target="_pod" href="/go&domain=' . $pod['domain'] . '">' . $pod['metatitle'] . '</a></div>';
        echo '</div><div class="row ps-1 ms-2">';
        echo '<div class="col text-dark">' . $pod['metadescription'] . '</div>';
        echo '</div><div class="row col-lg-auto p-1 m-2 text-secondary small">';
        echo '<div class="col">' . $t->trans('base.strings.search.software') . ': <b>' . $pod['softwarename'] . '</b><span class="d-inline-block fa fa-circle ps-2 pe-2"></span>';
        echo $t->trans('base.strings.search.open') . ': <b>' . ($pod['signup'] ? $t->trans('base.general.yes') : $t->trans('base.general.no')) . '</b><span class="d-inline-block fa fa-circle ps-2 pe-2"></span>';
        echo $t->trans('base.strings.search.location') . ': <b>' . $pod['countryname'] . '</b><span class="d-inline-block fa fa-circle ps-2 pe-2"></span>';
        echo $t->trans('base.strings.search.language') . ': <b>' . ($pod['detectedlanguage'] ? $iso->languageByCode1($pod['detectedlanguage']) : '') . '</b></div>';
        echo '<div class="mb-5"></div>';
        echo '</div>';
    }
    echo '</div>';
    podLog('Search for ' . $query);
} else {
    echo 'No search results<br>';
}
