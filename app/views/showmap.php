<?php

/**
 * Show map of pods.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

$lat   = ipLocation($_SERVER['REMOTE_ADDR'])['lat'] ?? 31;
$long  = ipLocation($_SERVER['REMOTE_ADDR'])['long'] ?? -99;

$subdomain  = join('.', explode('.', $_SERVER['HTTP_HOST'], -2));
$software_all    = !empty($subdomain) ? ucwords($subdomain) : 'All';

$pods = allServersList($subdomain);

$softwaren = !empty($subdomain) ? '&software=' . $subdomain : '';

if ($_SERVER['CDN']) {
    echo '
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet@1/dist/leaflet.min.css">
    <script src="https://cdn.jsdelivr.net/combine/npm/leaflet@1,npm/leaflet.markercluster@1"></script>
    ';
} else {
    echo '
    <link rel="stylesheet" href="/node_modules/leaflet/dist/leaflet.css"/>
    <script src="/node_modules/leaflet/dist/leaflet.js"></script>
    <script type="text/javascript" src="/node_modules/leaflet.markercluster/dist/leaflet.markercluster.js"></script>
    ';
}
?>

<div class="container-fluid">
    <div class="row">
    <div class="col-1">
        <?php echo '<a href="/go' . $softwaren . '" class="m-1 btn btn-sm bg-blue text-white">' . $t->trans('base.strings.list.navs.auto') . '</a>' ?>
    </div>
    <div class="col d-flex justify-content-center">
        <?php echo '<h2>' . $t->trans('base.strings.map.title') . ' ' . $software_all . '</h2>' ?>
    </div>
    </div>
</div>
<div class="shadow" id="map"></div>
<?php echo '<small>' . $t->trans('base.strings.map.tip')  . '</small>' ?>
<script type="text/javascript">
    var geoJsonData = {
        'type': 'FeatureCollection',
        'features': [
            <?php

            $i = 0;
            foreach ($pods as $pod) {
                // If this isn't the first entry, put a comma to separate the entries.
                $i++ > 0 && print ',';
                $pod_name     = (preg_replace('/[^\p{L}\p{N} ]+/', '', $pod['name']) ?? '') . ' ' . $pod['domain'];
                $pod_software = preg_replace('/[^\p{L}\p{N} ]+/', '', $pod['softwarename']) ?? '';
                $signup       = $pod['signup'] ? 'yes' : 'no';
                $pointlat     = $pod['long'] ?? '41';
                $pointlong    = $pod['lat'] ?? '-110';
                echo <<<EOF
{
  'type': 'Feature',
  'id': '1',
  'properties' : {
    'html': '<a href="/go&domain={$pod['domain']}">$pod_name</a><a href="/{$pod['domain']}"><span style="font-size: 1.4em; opacity: 0.2;" class="fa fa-info-circle align-top ps-3"></span></a><br>{$t->trans('base.strings.list.columns.software')}: {$pod_software}<br>{$t->trans('base.strings.list.columns.signups')}: $signup<br>{$t->trans('base.strings.list.columns.months')}: {$pod['monthsmonitored']} <br>{$t->trans('base.strings.list.columns.users')}: {$pod['total_users']}<br>{$t->trans('base.strings.list.columns.uptime')}: {$pod['uptime_alltime']}%'
  },
  'geometry': {
    'type': 'Point',
    'coordinates': [{$pointlat},{$pointlong}]
  }
}
EOF;
            }
            ?>
        ]
    };
    var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 9,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    var map = L.map('map', {zoom: 5, center: [<?php echo $lat; ?>, <?php echo $long; ?>]}).addLayer(tiles);
    var markers = L.markerClusterGroup({
        maxClusterRadius: 33, animateAddingMarkers: true, iconCreateFunction: function (cluster) {
            return new L.DivIcon({html: '<b class="icon">' + cluster.getChildCount() + '</b>', className: 'mycluster', iconSize: L.point(35, 35)});
        }
    });
    var geoJsonLayer = L.geoJson(geoJsonData, {
        onEachFeature: function (feature, layer) {
            layer.bindPopup(feature.properties.html);
        }
    });
    markers.addLayer(geoJsonLayer);
    map.addLayer(markers);
</script>
