<?php

/**
 * This is just a single pod data pull for /domain.tld page
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

use RedBeanPHP\R;
use Carbon\Carbon;
use RedBeanPHP\RedException;

$input = isset($_GET['input']) ? substr($_GET['input'], 1) : null;
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

// Required parameters.
if ($input != null) {
    $_domain = strtolower(idn_to_utf8($input));
} else {
    die('no domain given');
}

$iso = new Matriphe\ISO639\ISO639();

try {
    $pod = R::getRow('
        SELECT domain, softwarename, signup, daysmonitored, score, name, servertype, countryname, city, state, detectedlanguage, date_created, date_laststats, status, metatitle, metadescription, shortversion
        FROM pods
        WHERE domain = ?
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
    ', [$_domain, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $domain_clicks = R::getAll("
        SELECT
            to_char(date_clicked, 'yyyy-mm') AS yymm,
            SUM(manualclick) AS manualclick, 
            SUM(autoclick) AS autoclick
        FROM clicks
        WHERE domain = ?
        GROUP BY yymm
        ORDER BY yymm
    ", [$_domain]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
if ($pod['domain'] && $pod['status'] < 3) {
    $humanmonitored = Carbon::createFromTimeStamp(strtotime($pod['date_created']))->locale($locale->language)->diffForHumans();
    ?>
    <input type="hidden" name="domain" value="<?php echo $pod['domain']; ?>">
    <div class="accordion" id="accordionExample"><div class="body">
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        <strong><?php echo $t->trans('base.strings.singlepage.about') . ' ' . $pod['domain']; ?></strong>
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                    <?php
                    echo '<div class="text-justify row row-cols-1 p-2 w-100"><h5><a href="/go&domain=' . $pod['domain'] . '">' . $pod['metatitle'] .  '</a></h5>';
                    echo '<p class="fw-bold">' . $pod['metadescription'] . '</p>';
                    echo $t->trans('base.strings.singlepage.version', ['%(domain)' => $pod['domain'], '%(software)' => $pod['softwarename'], '%(version)' => $pod['shortversion']]);
                    echo '<br>';
                    echo $t->trans('base.strings.singlepage.status', ['%(daysmonitored)' => $humanmonitored, '%(score)' => $pod['score'], '%(maxscore)' => '100']);
                    echo '<br>';
                    echo $t->trans('base.strings.singlepage.language', ['%(language)' => $pod['detectedlanguage'] ? $iso->languageByCode1($pod['detectedlanguage']) : '']);
                    echo '<br>';
                    if ($pod['servertype'] == 'cloudflare') {
                        echo $t->trans('base.strings.singlepage.private');
                    } elseif ($pod['countryname']) {
                        echo $t->trans('base.strings.singlepage.location', ['%(location)' => $pod['city'] . ' ' . $pod['state'] . ' ' . $pod['countryname']]);
                    } else {
                        echo $t->trans('base.strings.singlepage.nolocation');
                    }
                    echo '<br>';
                    $last_podcheck  = Carbon::createFromFormat('Y-m-d H:i:s', $pod['date_laststats'])->locale($locale->language)->diffForHumans(null, true);
                    echo '<br>' . $t->trans('base.strings.singlepage.lastchecked') . ' ' . $last_podcheck;
                    echo ' ago <br><br>';
                    echo ($pod['signup'] ? $t->trans('base.strings.singlepage.opensignup') : $t->trans('base.strings.singlepage.closedsignup'));
                    echo '</p></div>';
                    echo '<div class="w-100 p-2 row"></div>';
                    ?>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                        <strong><?php echo $t->trans('base.strings.singlepage.charts') ?></strong>
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body w-100 p-md-5">
                        <?php
                        $_GET['domain'] = $pod['domain'];
                        echo  '<div class="align-items-center row"><h5 class="fw-bold text-center">' . $t->trans('base.strings.singlepage.uptime') . '</h5></div><div class="d-flex w-100 p-md-5 align-items-center row">';
                        include 'podstat-uptime.php';
                        echo '</div><div class="align-items-center row"><h5 class="fw-bold text-center">' . $t->trans('base.strings.singlepage.userstats') . '</h5></div><div class="d-flex w-100 p-md-5 align-items-center row">';
                        include 'podstat-user-counts.php';
                        echo '</div><div class="align-items-center row"><h5 class="fw-bold text-center">' . $t->trans('base.strings.singlepage.actionstats') . '</h5></div><div class="d-flex w-100 p-md-5 align-items-center row">';
                        include 'podstat-actions-counts.php';
                        ?>
    </div>
    <div class="align-items-center row">
    <h5 class="fw-bold text-center"><?php echo $t->trans('base.strings.singlepage.clicksout') ?></h5>
    </div>
    <div class="align-items-center row">
        <div class="d-flex w-100 chart-container p-md-5">
            <canvas id="clicks"></canvas>
        </div>
    </div>
</div>
            </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                <button id="jsonrawclick" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                    <strong><?php echo $t->trans('base.strings.singlepage.data') ?></strong>
                </button>
            </h2>
            <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                <pre id="jsonraw"><?php echo $t->trans('base.strings.singlepage.query') ?></pre>
                </div>
            </div>
        </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="panelsStayOpen-headingFour">
            <button id="jsonerrorclick" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false" aria-controls="panelsStayOpen-collapseFour">
                <strong><?php echo $t->trans('base.strings.singlepage.errors') ?></strong>
            </button>
        </h2>
        <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingFour" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <pre id="jsonerror"><?php echo $t->trans('base.strings.singlepage.query') ?></pre>
            </div>
        </div>
    </div>
    </div>
<script>
    new Chart(document.getElementById('clicks'), {
        type: "bar",
        data: {
            labels: <?php echo json_encode(array_column($domain_clicks, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($domain_clicks, 'manualclick')); ?>,
                label: 'Manual',
                fill: false,
                yAxisID: "l2",
                borderColor: "#A07614",
                backgroundColor: "#A07614",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($domain_clicks, 'autoclick')); ?>,
                label: 'Auto',
                fill: false,
                yAxisID: "l2",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 4,
                pointHoverRadius: 6
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                l2: {
                    position: "left",
                }
            }
        }
    });
    </script>
    <?php
} elseif ($pod['domain']  && $pod['status'] > 3) {
    ?>
    <div class="container">
        <div class="text-justify row row-cols-1 p-2">
            <h3><?php echo $t->trans('base.strings.singlepage.deleted') ?></h3>
            <br><h4><?php echo $t->trans('base.strings.singlepage.findother', ['%(software)' => $pod['softwarename']]) ?></h4>
            <br><a href="https://<?php echo $pod['softwarename'] . '.' . $_SERVER['DOMAIN'] ?>"><?php echo $pod['softwarename'] . '.' . $_SERVER['DOMAIN'] ?></a>
        </div>
    </div>
    <?php
} else {
    podLog('Singlepage Missing', $_domain, 'warning');
    ?>
    <div class="container">
        <div class="text-justify row row-cols-1 p-2">
        <h1><?php echo $t->trans('base.strings.singlepage.notfound') ?></h1>
            <a href="/?domain=<?php echo $_domain ?>&add=&action=save"><?php echo $t->trans('base.strings.singlepage.addit') ?></a>
        </div>
    </div>
    <?php
}
