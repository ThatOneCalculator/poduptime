<?php

/**
 * Show pod list table.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

$country_code = ipLocation($_SERVER['REMOTE_ADDR'])['country'];

$subdomain = join('.', explode('.', $_SERVER['HTTP_HOST'], -2));
$softwaren = !empty($subdomain) ? '&software=' . $subdomain : '';
$serverlocation  = $_SERVER['SERVER_LOCATION'];

?>
<div class="d-flex">
<div class="justify-content-start order-1 me-auto">
    <?php
    if (empty($subdomain)) {
        echo '<a href="/go" class="m-1 btn btn-sm bg-blue text-white">' . $t->trans('base.strings.list.navs.auto') . '</a>';
        echo '<a href="#" class="m-1 btn btn-sm bg-blue text-white wizardstart" data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-title="' . $t->trans('base.strings.wizard.title') . '" data-url="app/views/wizard.php?page=1">' . $t->trans('base.strings.list.navs.wizard') . '</a>';
    } else {
        echo '<a href="/go' . $softwaren . '" class="m-1 btn btn-sm bg-blue text-white">' . $t->trans('base.strings.list.navs.auto') . '</a>';
        echo '<a href="/map" class="m-1 btn btn-sm bg-blue text-white">' . $t->trans('base.strings.list.navs.map') . '</a>';
    }
    ?>
</div>
<div class="justify-content-end order-3 ms-auto">
    <button type="button" class="columnssimple hidden m-1 btn btn-sm bg-blue text-white"><?php echo $t->trans('base.strings.list.navs.basic') ?></button>
    <div class="columnSelectorWrapper m-1">
        <input id="colSelect1" type="checkbox" class="hidden">
        <label class="columnSelectorButton m-1 btn btn-sm bg-blue text-white" for="colSelect1"><?php echo $t->trans('base.strings.list.navs.select') ?></label>
        <div id="columnSelector" class="dropdown columnSelector">
        </div>
    </div>
    <button type="button" class="resetfilters m-1 btn btn-sm bg-blue hidden text-white"><?php echo $t->trans('base.strings.list.navs.reset') ?></button>
    <button type="button" class="d-none d-md-inline columnsadvanced m-1 btn btn-sm bg-blue text-white"><?php echo $t->trans('base.strings.list.navs.all') ?></button>
</div>
    <div class="pager hidden justify-content-center order-2">
        <span class="first pagination" title="First page">&laquo;</span>
        <span class="prev pagination" title="Previous page">&lt;</span>
        <span class="pagedisplay text-center pagerhidden"></span>
        <span class="next pagination" title="Next page">&gt;</span>
        <span class="last pagination" title="Last page">&raquo;</span>
        <div class="d-inline-block">
            <select class="pagesize hidden form-control form-control-sm">
                <option value="15">15</option>
                <option value="60">60</option>
                <option value="120">120</option>
                <option value="240">240</option>
                <option value="500">500</option>
                <option value="all">All</option>
            </select>
        </div>
    </div>
</div>
<div class="table-responsive">
    <div class="loadingtable">
        <div class="loadingmessage">
            <?php
            if ($subdomain) {
                echo $t->trans('base.strings.list.loading');
            } else {
                echo $t->trans('base.strings.list.loadingall');
            };
            ?>
        </div>
        <div class="spin spinner-border" role="status">
            <span class="sr-only"></span>
        </div>
    </div>
    <table class="table table-bordered table-sm tablesorter table-hover tfont">
        <thead class="thead">
        <tr>
            <th style="width: 325px;" data-priority="1" class="columnSelector-disable"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.serverdesc') ?>"><?php echo $t->trans('base.strings.list.columns.server') ?></div></th>
            <th class="columnSelector-false"><div><?php echo $t->trans('base.strings.list.columns.name') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.versiondesc') ?>"><?php echo $t->trans('base.strings.list.columns.version') ?></div></th>
            <th class="columnSelector-false  filter-select"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.softwaredesc') ?>"><?php echo $t->trans('base.strings.list.columns.software') ?></div></th>
            <th data-priority="1"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.uptimedesc') ?>"><?php echo $t->trans('base.strings.list.columns.uptime') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.latencydesc', ['%(hostlocation)' => $_SERVER['SERVER_LOCATION']]) ?>"><?php echo $t->trans('base.strings.list.columns.latency') ?></div></th>
            <th class="filter-select" data-priority="2"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.signupsdesc') ?>"><?php echo $t->trans('base.strings.list.columns.signups') ?></div></th>
            <th data-priority="2"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.usersdesc') ?>"><?php echo $t->trans('base.strings.list.columns.users') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.users6desc') ?>"><?php echo $t->trans('base.strings.list.columns.users6') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.users1desc') ?>"><?php echo $t->trans('base.strings.list.columns.users1') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.postsdesc') ?>"><?php echo $t->trans('base.strings.list.columns.posts') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.commentsdesc') ?>" data-placeholder="Try: 10 - 50"><?php echo $t->trans('base.strings.list.columns.comments') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.monthsdesc') ?>" data-placeholder="Try: ! 0"><?php echo $t->trans('base.strings.list.columns.months') ?></div></th>
            <th class="columnSelector-false"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.scoredesc') ?>"><?php echo $t->trans('base.strings.list.columns.score') ?></div></th>
            <th class="columnSelector-false filter-select"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.statusdesc') ?>"><?php echo $t->trans('base.strings.list.columns.status') ?></div></th>
            <th class="filter-select" data-priority="2"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.countrydesc') ?>" data-placeholder="Try: <?php echo $country_code ?: 'US'; ?>"><?php echo $t->trans('base.strings.list.columns.country') ?></div></th>
            <th class="columnSelector-false" data-priority="5"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.citydesc') ?>"><?php echo $t->trans('base.strings.list.columns.city') ?></div></th>
            <th class="columnSelector-false" data-priority="5"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.statedesc') ?>"><?php echo $t->trans('base.strings.list.columns.state') ?></div></th>
            <th data-priority="2" class="filter-select"><div data-toggle="tooltip" title="<?php echo $t->trans('base.strings.list.columns.languagedesc') ?>"><?php echo $t->trans('base.strings.list.columns.language') ?></div></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?php echo $t->trans('base.general.close') ?></button>
            </div>
        </div>
    </div>
</div>
