<div class="container-fluid">
    <h1 class="text-center"><?php echo $t->trans('base.strings.stats.title') ?></h1>
    <div class="row justify-content-center">
        <div class="col-lg-2 justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.stats.users') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_network_users"></canvas>
            </div>
        </div>
        <div class="col-lg-2  justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.stats.serverss') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_network_pods"></canvas>
            </div>
        </div>
        <div class="col-lg-2 justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.stats.serversc') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_servers_country"></canvas>
            </div>
        </div>
    </div>
    <br>
    <h1 class="text-center p-1"><?php echo $t->trans('base.strings.stats.statsfor') . ' ' ?> <?php echo $software_all ?></h1>
        <div class="row justify-content-center p-1">
            <div class="d-flex w-100 p-md-5 justify-content-center">
                <h4><?php echo $t->trans('base.strings.stats.average') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.growth') ?></h4>
            </div>
            <div class="d-flex w-100 p-md-5 justify-content-center">
                <canvas class="d-flex w-100 p-md-5 justify-content-center" id="user_growth"></canvas>
            </div>
        </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.stats.average') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.growthactive') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="user_growth_active"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.stats.average') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.serversper') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="pod_growth"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.stats.average') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.commentsper') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="comment_growth"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.stats.average') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.postsper') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="posts_growth"></canvas>
        </div>
    </div>
    <?php echo '<small>' . $t->trans('base.strings.stats.tip')  . '</small>' ?>
</div>
