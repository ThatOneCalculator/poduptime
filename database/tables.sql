CREATE TABLE pods (
 id serial8 UNIQUE PRIMARY KEY,
 domain text UNIQUE NOT NULL,
 name text,
 metatitle text,
 metadescription text,
 metanodeinfo text,
 metalocation text,
 owner text,
 metaimage text,
 onion text,
 i2p text,
 terms text,
 pp text,
 support text,
 camo text,
 zipcode text,
 softwarename text,
 masterversion text,
 fullversion text,
 shortversion text,
 score int DEFAULT 50,
 weightedscore numeric(5,2) DEFAULT 0,
 ip text,
 detectedlanguage text,
 country text,
 countryname text,
 city text,
 state text, 
 lat text,
 long text,
 email text,
 ipv6 text,
 sslvalid text,
 monthsmonitored int,
 daysmonitored int,
 signup boolean,
 total_users int, 
 active_users_halfyear int,
 active_users_monthly int,
 local_posts int,
 uptime_alltime numeric(5,2),
 status smallint DEFAULT 1,
 latency smallint,
 service_xmpp boolean,
 services jsonb,
 protocols jsonb,
 token text,
 publickey text,
 tokenexpire timestamp,
 podmin_statement text,
 podmin_notify boolean,
 podmin_notify_level int DEFAULT 90,
 sslexpire timestamp,
 dnssec boolean,
 servertype text,
 comment_counts int,
 weight int DEFAULT 10,
 date_updated timestamp DEFAULT current_timestamp,
 date_laststats timestamp DEFAULT current_timestamp,
 date_created timestamp DEFAULT current_timestamp
);

CREATE TABLE clicks (
 id serial8 UNIQUE PRIMARY KEY,
 domain text,
 manualclick int,
 autoclick int,
 date_clicked timestamp DEFAULT current_timestamp
);

CREATE TABLE checks (
 id serial8 UNIQUE PRIMARY KEY,
 domain text,
 online boolean,
 error text,
 latency numeric(8,6),
 total_users int,
 active_users_halfyear int,
 active_users_monthly int,
 local_posts int,
 comment_counts int,
 shortversion text,
 version text,
 date_checked timestamp DEFAULT current_timestamp
);

CREATE TABLE masterversions (
 id serial8 UNIQUE PRIMARY KEY,
 software text,
 version text,
 devlastcommit timestamp,
 releasedate timestamp,
 date_checked timestamp DEFAULT current_timestamp
);

CREATE TABLE monthlystats (
 id serial8 UNIQUE PRIMARY KEY,
 softwarename text,
 total_users int,
 total_active_users_halfyear int,
 total_active_users_monthly int,
 total_posts int,
 total_comments int,
 total_pods int,
 total_uptime int,
 date_checked timestamp DEFAULT current_timestamp
);

CREATE TABLE meta (
 id serial8 UNIQUE PRIMARY KEY,
 name text,
 value text,
 date_created timestamp DEFAULT current_timestamp
);

INSERT INTO meta (name) VALUES('languages_updated');
INSERT INTO meta (name) VALUES('cacert_updated');
INSERT INTO meta (name) VALUES('geoip_updated');
INSERT INTO meta (name) VALUES('masterversions_updated');
INSERT INTO meta (name) VALUES('federation_updated');
INSERT INTO meta (name) VALUES('statstable_updated');
INSERT INTO meta (name) VALUES('backup');
INSERT INTO meta (name) VALUES('pods_updated');
INSERT INTO meta (name) VALUES('sitemap_updated');

CREATE EXTENSION IF NOT EXISTS cube CASCADE;
CREATE EXTENSION IF NOT EXISTS postgis CASCADE;
CREATE EXTENSION IF NOT EXISTS earthdistance CASCADE;