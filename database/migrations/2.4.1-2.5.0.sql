CREATE TABLE meta (
 id serial8 UNIQUE PRIMARY KEY,
 name text,
 value text,
 date_created timestamp DEFAULT current_timestamp
);
INSERT INTO meta (name) VALUES('languages_updated');
INSERT INTO meta (name) VALUES('cacert_updated');
INSERT INTO meta (name) VALUES('geoip_updated');
INSERT INTO meta (name) VALUES('masterversions_updated');
INSERT INTO meta (name) VALUES('federation_updated');
INSERT INTO meta (name) VALUES('statstable_updated');
INSERT INTO meta (name) VALUES('backup');
INSERT INTO meta (name) VALUES('pods_updated');
