<?php

use Siler\GraphQL;
use Siler\Http\Request;
use Siler\Http\Response;

require_once __DIR__ . '/../boot.php';

Response\cors();

if (Request\method_is('post')) {
    $schema = include __DIR__ . '/schema.php';
    try {
        GraphQL\init($schema);
    } catch (Exception $e) {
    }
} else {
    require 'graphiql.php';
}
