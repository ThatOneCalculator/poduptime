<?php

use RedBeanPHP\R;
use Poduptime\PodStatus;

use function Siler\{array_get_str};

require_once __DIR__ . '/../boot.php';

$queryType = [
    'nodes' => function ($_, array $args) {
        $sql = '
        SELECT 
         id,
         domain,
         name,
         metatitle,
         metadescription,
         metanodeinfo,
         metalocation,
         owner,
         metaimage,
         onion,
         i2p,
         terms,
         pp,
         support,
         camo,
         zipcode,
         softwarename,
         masterversion,
         fullversion,
         shortversion,
         score,
         ip,
         detectedlanguage,
         country,
         countryname,
         city,
         state, 
         lat,
         long,
         ipv6,
         sslvalid,
         monthsmonitored,
         daysmonitored,
         signup,
         total_users, 
         active_users_halfyear,
         active_users_monthly,
         local_posts,
         uptime_alltime,
         status,
         latency,
         service_xmpp,
         services,
         protocols,
         podmin_statement,
         podmin_notify,
         podmin_notify_level,
         sslexpire,
         dnssec,
         servertype,
         comment_counts,
         weight,
         date_updated,
         date_laststats,
         date_created
        FROM pods
    ';
        $softwarename = array_get_str($args, 'softwarename', '');
        $status       = PodStatus::getAll()[array_get_str($args, 'status', '')] ?? null;
        if ($softwarename !== '' && isset($status)) {
            $sql .= ' WHERE softwarename = ? AND status = ?';
            return R::getAll($sql, [$softwarename, $status]);
        } elseif ($softwarename !== '') {
            $sql  .= ' WHERE softwarename = ?';
            return R::getAll($sql, [$softwarename]);
        } elseif (isset($status)) {
            $sql .= ' WHERE status = ?';
            return R::getAll($sql, [$status]);
        } else {
            return R::getAll($sql);
        }
    },
    'node' => function ($_, array $args) {
        $domain = array_get_str($args, 'domain');
        return R::getAll('
        SELECT 
         id,
         domain,
         name,
         metatitle,
         metadescription,
         metanodeinfo,
         metalocation,
         owner,
         metaimage,
         onion,
         i2p,
         terms,
         pp,
         support,
         camo,
         zipcode,
         softwarename,
         masterversion,
         fullversion,
         shortversion,
         score,
         ip,
         detectedlanguage,
         country,
         countryname,
         city,
         state, 
         lat,
         long,
         ipv6,
         sslvalid,
         monthsmonitored,
         daysmonitored,
         signup,
         total_users, 
         active_users_halfyear,
         active_users_monthly,
         local_posts,
         uptime_alltime,
         status,
         latency,
         service_xmpp,
         services,
         protocols,
         podmin_statement,
         podmin_notify,
         podmin_notify_level,
         sslexpire,
         dnssec,
         servertype,
         comment_counts,
         weight,
         date_updated,
         date_laststats,
         date_created
        FROM pods
        WHERE domain = ?
    ', [$domain]);
    },
    'checks' => function ($_, array $args) {
        $limit = array_get_str($args, 'limit', 5);
        $domain = array_get_str($args, 'domain', '');
        $online = array_get_str($args, 'online', '');
        $sql = '
        SELECT id, domain, online, error, latency, total_users, active_users_halfyear, active_users_monthly, local_posts, comment_counts, shortversion, version, date_checked
        FROM checks
        ';
        if ($domain && $online) {
            $sql .= ' WHERE domain = ? AND online = ? ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$domain, $online, $limit]);
        } elseif ($domain) {
            $sql .= ' WHERE domain = ? ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$domain, $limit]);
        } else {
            $sql .= ' ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$limit]);
        }
    },
    'clicks' => function ($_, array $args) {
        $limit = array_get_str($args, 'limit', 5);
        $domain = array_get_str($args, 'domain', '');
        $sql = '
        SELECT id, domain, manualclick, autoclick, date_clicked
        FROM clicks
        ';
        if ($domain) {
            $sql .= ' WHERE domain = ? ORDER BY date_clicked DESC LIMIT ?';
            return R::getAll($sql, [$domain, $limit]);
        } else {
            $sql .= ' ORDER BY date_clicked DESC LIMIT ?';
            return R::getAll($sql, [$limit]);
        }
    },
    'monthlystats' => function ($_, array $args) {
        $sql = '
        SELECT id, softwarename, total_users, total_active_users_halfyear, total_active_users_monthly, total_posts, total_comments, total_pods, total_uptime, date_checked
        FROM monthlystats
        ';
        $softwarename = array_get_str($args, 'softwarename', '');
        $mostcurrent  = array_get_str($args, 'mostcurrent', '');
        if ($softwarename !== '' && $mostcurrent == 'true') {
            $sql .= ' WHERE softwarename = ? ORDER BY date_checked DESC LIMIT 1';
            return R::getAll($sql, [$softwarename]);
        } elseif ($softwarename !== '') {
            $sql  .= ' WHERE softwarename = ? ORDER BY date_checked DESC';
            return R::getAll($sql, [$softwarename]);
        } elseif ($mostcurrent == 'true') {
            $mostcurrentnumber = R::getAll('
             SELECT distinct COUNT(softwarename), date_checked
             FROM monthlystats
             WHERE softwarename != \'\'
             GROUP by date_checked order by date_checked desc limit 1
             ');
            $sql  .= ' ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$mostcurrentnumber[0]["count"]]);
        } else {
            $sql  .= ' ORDER BY date_checked DESC';
            return R::getAll($sql);
        }
    },
    'masterversions' => function () {
        return R::getAll('
        SELECT id, software, version, devlastcommit, releasedate, date_checked
        FROM masterversions
        ORDER BY id DESC
    ');
    },
    'softwares' => function () {
        return R::getAll('
        SELECT distinct softwarename, COUNT(id)
        FROM pods
        WHERE softwarename != \'\' AND status < 3
        GROUP by softwarename
    ');
    },
];

return [
    'Query'      => $queryType,
];
