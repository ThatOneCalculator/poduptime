<?php

/**
 * Main API welcome screen.
 */

declare(strict_types=1);

require_once __DIR__ . '/../boot.php';

?>
<html>
  <head>
    <title><?php echo c('longtitle') ?> GraphiQL API</title>
    <link href="/node_modules/graphiql/graphiql.min.css" rel="stylesheet" />
  </head>
  <body style="margin: 0;">
    <div id="graphiql"></div>

    <script
      src="/node_modules/react/umd/react.production.min.js"
    ></script>
    <script
      src="/node_modules/react-dom/umd/react-dom.production.min.js"
    ></script>
    <script
      src="/node_modules/graphiql/graphiql.min.js"
    ></script>

    <script>
const customQuery =
`# Welcome to GraphiQL
#
# GraphiQL is an in-browser tool for writing, validating, and
# testing GraphQL queries.
#
# Type queries into this side of the screen, and you will see intelligent
# typeaheads aware of the current GraphQL type schema and live syntax and
# validation errors highlighted within the text.
#
# Some examples at: https://gitlab.com/diasporg/poduptime/-/wikis/API
#
# An example GraphQL query might look like:
#
{
  node(domain: "diasp.org") {
    domain
    name
    metatitle
    metadescription
    metaimage
    onion
    terms
    pp
    softwarename
    daysmonitored
    monthsmonitored
    date_updated
    date_laststats
    date_created
    countryname
    lat
    long
    uptime_alltime
    latency
    sslexpire
    total_users
    active_users_monthly
    active_users_halfyear
    score
    status
    signup
  }
}
    `;
      const graphQLFetcher = graphQLParams =>
        fetch('', {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(graphQLParams),
        })
          .then(response => response.json())
          .catch(() => response.text());
      ReactDOM.render(
        React.createElement(GraphiQL, { fetcher: graphQLFetcher, defaultQuery: customQuery }),
        document.getElementById('graphiql'),
      );
    </script>
  </body>
</html>
