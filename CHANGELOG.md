# Changelog
The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]
### Fixed
### Added
### Changed
- note when no hits on welcome page
- padding on charts
### Deprecated
### Removed
### Security

## [3.9.0]
:exclamation:  DB migrations required! (see [SQL migration script][3.9.0-sql-migration])
### Fixed
- save server fixed
- cc on email fixed
- casing fix on singlepage https://gitlab.com/diasporg/poduptime/-/issues/45
- no status set on bad json and server code 200 https://gitlab.com/diasporg/poduptime/-/issues/47
### Added
- update API push to friendica/mastodon 
- update API to allow for montlystats to filter to software and mostcurrent result https://gitlab.com/diasporg/poduptime/-/issues/49
- API call for softwares
### Changed
- don't show singlepage to status user deleted or system deleted
- less confusing errors in checks table
### Deprecated
### Removed
### Security

## [3.8.1]
### Fixed
- RSS Url fix
### Added
- raw data from api on singlepage view
- database table cleanup for dead servers - probably use: ALTER TABLE checks SET (autovacuum_enabled = off); first time on run if data
- basic terms and pp page
### Changed
- move server owner statement from list to singleview to shorten ajex call
- remove ipv6, dnssec, services, protocols from list to shorten ajax call, these are available via API
- rename backup.php to database.php - update cronjobs
- limit pie chats to top 10 as they are a mess to look at
- limit admin statement to 500 char
### Deprecated
- html in server admin statement
### Removed
### Security

## [3.8.0]
:exclamation:  DB migrations required! (see [SQL migration script][3.8.0-sql-migration])
### Fixed
- allow idn domain to give ascii nodeinfo link even tho its not same origin
### Added
- mark servers hidden behind cloudflare location private since we can not detect their location
- better chart hovering - thanks for  MR @CristianAUnisa
- set duration of rss new servers in .env
### Changed
- mark servers hidden behind cf on single page with warding
- cleanup long menu links in footer throwing off mobile UI
### Deprecated
### Removed
### Security

## [3.7.0]
:exclamation:  DB migrations required! (see [SQL migration script][3.7.0-sql-migration])
### Fixed
- cleandomain function for invalid punycode
- map view with odd code used in names and softwarenames that break the array
### Added
- more notes on status
- add halfear and monthy active user stats
### Changed
- API URL in .env
- combine helper scripts, see README, update your cron tasks
### Deprecated
- crawl.php and function, this never worked right,the fediverse is too big now. use config/app.php and add json node strings where you like
### Removed
### Security

## [3.6.2]
### Fixed
### Added
### Changed
- validate domains and clean domains on own, the validator packages are not up to date on idn
### Deprecated
### Removed
### Security

## [3.6.1]
### Fixed
- i8n domains in UI and scripts
- this fixes sitemap generator also as that was a idn domain issue
### Added
- convert punycode to utf8 on add. No DB migration for this as DB alredy has dupes. migrate using something like https://www.charset.org/punycode
### Changed
- separate logs for server and human actions
- library changes and updates for i8n, run yarn and composer updates
### Deprecated
### Removed
### Security

## [3.6.0]
:exclamation:  DB migrations required! (see [SQL migration script][3.6.0-sql-migration])
### Fixed
- Auto link on map view
- hacky fix in index.php for space in software name https://gitlab.com/diasporg/poduptime/-/issues/35
### Added
- collect and report on 1 and 6 month user data
- display 1 and 6 month user stats on singlepage, could be used on stats page down the road with data over time
- help text in .env for podminedit page
- show this software version on status page
### Changed
- domain name in single page header
### Deprecated
### Removed
### Security

## [3.5.7]
### Fixed
- bump translator to 6 for some bugs in fallback
- bug in monthly stats for posts and users
### Added
- gitea for masterversion check
### Changed
### Deprecated
### Removed
### Security


## [3.5.6]
### Fixed
- Welcome screen dupe
### Added
- Translate Admin and other areas
- add some new softwares to config
### Changed
- cleanup right click on list
### Security
- htmlspecialchars on add and edit inputs for domain

## [3.5.4]
### Fixed
### Added
- link to crowdin https://crowdin.com/project/poduptime
### Changed


## [3.5.3]
### Fixed
- Small translation items


## [3.5.1]
### Fixed
- min score fixed.
### Added
- RSS feed generator
- translations, ML started, user fixed at https://translate.diasp.org
### Changed
- Use months online as part of the weightedscore


## [3.5.0]
### Added
- SMTP sendmail, configure in .env
### Fixed
- backup retention works now
- show podmin error note
- TXT DNS record check for owners
- chartjs local pull corrected
### Changed
- boostrap from 4 to 5
- chartjs from 2 to 3
- using more bootstrap for graphing to fill screens

## [3.4.1]
### Fixed
- crawler valides nodeinfo same as add.php

## [3.4.0]
:exclamation:  DB migrations required! (see [SQL migration script][3.4.0-sql-migration])
### Changed
- directory structor changes, cronjpb updates needed!
### Added
- use CDN in .env file to serve assets from jsdelivr.net
- use geo in welcome to suggest a good server. db migration and php package updates.

## [3.3.0]
:exclamation:  config.php migrated to .env and config/app.php  
:exclamation:  DB migrations required! (see [SQL migration script][3.3.0-sql-migration])
### Added
- more things moved to /lib
- timeout can be global set in .env as optional override to 30 seconds
- respect blacklists for all views
- log things rather than bloat the db, check /log permissions
- support ipv6 only servers
- language_refresh in .env
- stale_older in .env
- stale_newer in .env
- masterversion_refresh in .env
- env in config.php !required
- DNS backup
### Changed
- db/update-remote-data.php no longer called from scripts, suggest a cronjob
- db/update-sitemap.php no longer called from scripts, suggest a cronjob
- db/update-monthly-stats.php no longer called from scripts, suggest a cronjob
- db/backup.php no longer called from scripts, suggest a cronjob
- move update-masterversions.php to lib/functionsremote.php and add test
- column ipv6 in pods table is text now and stores the address
### Deprecated
- !!!config.php
- user ratings, low usage, bad UX, no desire to improve
- apikeys table, never used
- update.php can no long check all, only 1, update-all.php is for all
- update.php no longer supports pgp-cgi
### Removed
- old db junk data
### Fixed
- masterversions check if git repo dead, also ckick this to log.
### Security
- use a DNS key the world can't guess

## [3.2.0]
:exclamation: config.php changes required!!
### Added
- config file used for db/update-remote data, update :config.php:
- config file used for db/update, update :config.php:
- link to software information from :config.php:
- check unlimited sites for server instances :config.php:
- documentation for api in the api
- cleanup nodes API call to allow some filters
- respect robots.txt for metadata from pages html code, update :config.php:
- all dig and curl use the dnsserver in config to allow for HNS or sub dns setups
### Changed
- only up pods on sitemap.xml
### Deprecated
- php7.4 and any libraries without 8.0 support
- gnusocial  on welcome page - been 3 years since any dev work - these should not be hardcoded
- maxmind mmdb updates. Update that as you like. https://github.com/maxmind/geoipupdate
### Removed 
### Fixed
- dig functions all the same
### Security

## [3.1.0]
:exclamation:  DB migrations required! (see [SQL migration script][3.1.0-sql-migration])
### Added
- new fields added to api
- basic tests and ci setup in gitlab.com
- basic stats for usage, search, browser, wizard
- store more data for future use and search query to use
- zip code fuzzy search support
- store terms, privacy and support links from owner (not displayed yet)
### Changed
- 3rd pie chart changed to be more useful data on /stats
- localhost is considered cli for testing
### Deprecated
- mapbox support
### Removed
- all results from search, add options for dead date later if useful, stay clean and useful
### Fixed
- link to ca cert updated
- meta tags store properly
### Security

## [3.0.0]
:exclamation: config.php changes required!!  
:exclamation:  DB migrations required! (see [SQL migration script][3.0.0-sql-migration])
### Added
- global search functionality
### Changed
- blacklist of domains and software are now text files to support some automation of bad things
### Deprecated
### Removed
### Fixed
- seo issues with tags
- domain in sitemap generator
### Security
- update yarn and composer package versions

## [2.9.4]
### Added
- autostart wizard modal when list?wizard in url with js
### Changed
- simplify the welcome screen even more
- update geoip at least once a week to match maxmind updates
### Deprecated
### Removed
### Fixed
- https://gitlab.com/diasporg/poduptime/-/issues/3 hidden domains no longer in go.php
### Security

## [2.9.3]
### Added
- Long and Short site name in config
### Changed
- Change language from pod to server
### Deprecated
### Removed
### Fixed
- Small UX issues
### Security

## [2.9.2]
### Added
- welcome page to help users learn to use the site
### Changed
- Call the table a list 
- move links to footer and clean the header
- modal when you click a pod
- cleanup single pod page
### Deprecated
### Removed
### Fixed
### Security

## [2.9.1]
### Added
### Changed
- Map zoom level
- Map try and guess IP closer
- Cluster map markers better
- Change git repo
### Deprecated
### Removed
### Fixed
### Security

## [2.9.0]
:exclamation: `config.php` changed - requires updates  
### Added
- Show number of pods we check in the status page
- Limit menu by pod count to be able to not show softwares with just a few test pods. config.php change.
### Changed
- Small DNS change so you know why you might have a TXT record
### Deprecated
- All apiv1 support
### Removed
- All apuv1 files
### Fixed
### Security

## [2.8.2]
### Added
- API v2 using graphql and Siler framework
- graphiql web-ide
### Changed
- Tell robots not to follow links to pods
- Limit stats to 2019 further as old data is inconsistent
- yarn 2.0
### Deprecated
- yarn 1.0 support
- API v1 api.php
### Removed
### Fixed
- Show stats in proper order yymm
### Security

## [2.8.1]
### Added
- Allow filter on domain name in config.php
### Changed
- Update map tiles to match theme
- Filter bad domains and software from sitemap.xml
- User charts on singleview
- Link to singleview from table
### Deprecated
### Removed
### Fixed
### Security

## [2.8.0]
:exclamation:  DB migrations required! (see [SQL migration script][2.8.0-sql-migration])  
:exclamation: `config.php` changed - requires updates
### Added
- Try and match country on a random go.php call - cloudflare assumed
- List the software specific pages in the footer
- store monthly stats by software name also
- show stats by software
- show more about the software on software specific sites
### Changed
- Show wizard only when viewing all softwares
- Domain needs to be configured in config.php
- Configure sofwares for wizard in config.php with yes/no and description
- Change menus to be less movement and better for mobile
- Only show software specific to area chosen by end user via subdomain
### Deprecated
### Removed
### Fixed
- Wizard calls as it goes and sorts just a bit faster at the end
### Security

## [2.7.0] 
:exclamation:  DB migrations required! (see [SQL migration script][2.7.0-sql-migration])  
:exclamation: `config.php` changed - requires updates  
### Added
- Donation link to bottom of pages if set in config.php
- Server location in config.php
- Use dispatcher to do things in parallel not one by one, see config.php
- Support subdomain for software name highlight
- basic idn support, you need to install php7.x-intl
- podmin last few connection errors on edit page
- config when to stop checking a pod
### Changed
- Update PHP and packages
- Check git code less as it is buggy
- Make diaspora more a highlight again in the UI/UX
- Small changes to make things easier on the eyes on mobile and desktop
- Resort the menus to be more clear how to move around
- Less data points on simple view
### Deprecated
### Removed
### Fixed
- Pod checking on the last date code was updated to pod - SQL migration
- Remove ? from ajax query to allow better cache
- Strip off the extra - in versioning for friendica and all
### Security

## [2.6.4]
### Added
- maxmind license key in config.php - now required by maxmind
### Changed
- mapbox api url structure
### Deprecated
### Removed
### Fixed
- nodeinfo that does not follow the spec will lower score
### Security

## [2.6.3]
### Added
### Changed
- js for table no longer filters pods that are signup only, this is a 20 second improvment on page load
- update remote data less often to save update time
- Color scheme and default table colums to be more simple
### Deprecated
### Removed
### Fixed
- update remote pods fixed
- try and handle pleroma or others making up own nodeinfo spec. dirty fix for now
### Security

## [2.6.2] - 2019-11-24
### Added
- Total rows count o loading screen as that is growing
- Status page timeout value is user set in config.php
### Changed
- Update backup meta if backup is a success
### Deprecated
### Removed
### Fixed
- Updated json pull for mastodon not having the full nodeinfo data
- Fix geoip if IP is not real
### Security
- Updated packages

## [2.6.1] - 2019-06-30
### Added
### Changed
- only check pods once a month found in the wild via crawl
- curl function timeout can be changed from 15 second default
### Deprecated
### Removed
### Fixed
- Error out if detectlanguage.com issue vs die out on script
### Security

## [2.6.0] - 2019-04-13
:exclamation: DB migrations required! (see [SQL migration script][2.6.0-sql-migration])
### Added
- store protocols that pods support 
- hide software names you don't want on table/go - leaving in stats for now
### Changed
- add.php test for pod meta like update.php
- use meta table for updating monthly stats not the diasp.org hack
### Deprecated
### Removed
### Fixed
- init call on new setup sets up the needed items
- missing tables.sql item from version 2.5.2
- missing table on tables.sql
- link to osada repo updated
- use any valid nodeinfo pod
- softwares are in config.php now so it is not hardcoded to add and update git repos
- some UI issues with more than 1000 pods listed
- uptime and growth charts per pod to be unlimited timespan
- use dig command vs delv as ecdsa keys are not being detected properly by delv
- stats page human readabe dates/times
- system deleted fix
### Security

## [2.5.2] - 2019-01-29
:exclamation: DB migrations required! (see [SQL migration script][2.5.2-sql-migration])  
### Added
- osada masterversion check
- sitemap generator, config.php update needed
### Changed
- /domain.tld page is simple text vs a javascript table
- link to sitemap in robots.txt (hardcoded with domain podupti.me)
- test for official PHP 7.3 release
### Deprecated
- api-more.php calls. old droid app been dead for years
### Removed
### Fixed
- go.php will only use online pods
### Security

## [2.5.1] - 2018-12-09
### Changed
- use file_get_contents less and prefer curl and copy to make things more stable
### Fixed
- Podmins can check their pods again via edit

## [2.5.0] - 2018-12-07
:exclamation: DB migrations required! (see [SQL migration script][2.5.0-sql-migration])  
### Added
- Meta data table for status and misc storage
- go.php can take a software= parameter and do a random pod on only one software/network
### Changed
- Status page is now /status from /db/status.php
### Deprecated
- db/pull.sh is no longer needed or used, cron can use db/update.php directly
### Removed
- db/last.data can be deleted from your system
### Fixed
- Moved status to a new table vs flat file as updates do not work right if not run every hour
### Security

## [2.4.2] - 2018-10-18
### Fixed
- Fixed db setup calls for cleaner links

## [2.4.1] - 2018-10-18
:exclamation: DB migrations required! (see [SQL migration script][2.4.1-sql-migration])  
### Added
- Initial check-code test using GitLab CI
### Changed
- Pod wizard switch from language to users (#195)
- Files renamed to change wording from pull to update to be more clear (#190)
- Use SVG for icons
### Removed
- userurl remove from table in DB, was unused
### Fixed
- Users chart fix (#194)
- Dev branch detection (#185) (#193)
- User deletion fixed (#198)
- Jump to domain fixed (#192)
- Updated yarn and composer packages - update both on git pull

## [2.4.0] - 2018-09-03
:exclamation: DB migrations required! (see [SQL migration script][2.4.0-sql-migration])  
:exclamation: `config.php` rewritten - requires migration    
### Added
- Added bootstrapping to simplify initialisation of config and database
- Config syntax has changed to array style (#155)
- Added `pghost` config to set database port
- Added `CONTRIBUTING.md`
- Podmin can choose at what fail score to send the notice out
- Podmin email shares details on why pod is failing
- Only retrieve location data for remote servers / IPs
- Fontawesome for icons
### Changed
- Introduce proper changelog format (#189)
- Moved DB migration scripts into `db` folder
- Use Curl for all http calls
- Use filter dropdowns for pre-defined columns
- Use pretty URLs (see nginx.example)
- Open pod URLs in a new tab
- Use detectlanguage.com API for language guess
- Only use JSON data, ignore HTML when returned
- Allow curl redirect on home page check
- Score now goes to -5000 before a pod is removed so dead pods get checked a while then removed for good
- Move functions to dedicated file to allow reuse
- Backup script rewrite
- Store services as json array  
- Paging can be changed to some or all on advanced view
### Fixed
- Notify podmins just once at 50 when pod failing (#186)
- Add missing meta and PHP module requirements to `composer.json`


## [2.3.1] - 2018-08-05
### Added
- Podmins can link directly to their pod via `https://podupti.me/domain.name` for stats and to allow users to rate easier
- Wizard to help you filter the columns to what you need (#145)
- Cookie used to remember last pod you clicked
### Changed
- Now one table with a basic default view you can customize (#171)
- Switch to a library for country to lat long lookup
- Switch GeoIP from built in PHP to library and use newer Maxmind database file

## [2.3.0] - 2018-07-19
:exclamation: DB migrations required! (see [SQL migration script][2.2.0-sql-migration])
### Added
- Language is detected based on your homepage, edit your homepage to non-en if that is what you use
- Add development and release dates to `masterversions` table (#143)
- Store full country name, store days monitored each pod (#150)
- Store detectedlanguage (#144)
- Show version and update in full view cleaner (#143)
- Filter and search on the columns of data (#147)
- Paginate the results so they fit per page (#147)
### Changed
- Podmins can no longer access `db/pull.php` to test their pod, they can however get to a debug screen from the edit pod area
- Edit will send to email on file and be less delay, runner of site does not really have any way to verify email address
- Default new pods to `UP` to be checked
- Use the git API for release versions, check development releases on pods (#143)
- Move from [bower to yarn](https://bower.io/blog/2017/how-to-migrate-away-from-bower/) for packages
- Move to PHP 7.2 with strict typing
- Move to [Eslint compliance](https://eslint.org/docs/rules/)
- Move to [PSR-2 compliance](https://www.php-fig.org/psr/psr-2/)
- NOTE `config.php.example` change to full paths for 2 items!
- Show time as human readable everywhere (#150)
### Removed
- Unused `hidden` and `secure` columns (#140, #141)
### Fixed
- Rename table `rating_comments` to `ratingcomments` for redbean support (#146)
### Security
- Forbid access to files that should be CLI only (#152)

## [2.2.0] - 2018-05-12
:exclamation: DB migrations required! (see [SQL migration script][2.2.0-sql-migration])
### Added
- Podmins can now pause/unpause or delete from podmin area
- Graph on user growth on the network
- Add monthly stats table
### Changed
- `go.php` auto select picks a more stable pod than before
- Make map prettier
- Use lines on tables to make them more readable
- Don't delete dead pods, keep them and data for history hide them for users
- Put daily tasks in the `pull.sh` and run each day
- Update status to 1-5 rather than text
### Fixed
- Fix ipv6

[3.9.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.8.0-3.9.0.sql
[3.8.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.7.0-3.8.0.sql
[3.7.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.6.2-3.7.0.sql
[3.6.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.4.0-3.6.0.sql
[3.4.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.3.0-3.4.0.sql
[3.3.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.1.0-3.3.0.sql
[3.1.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/3.0.0-3.1.0.sql
[3.0.0-sql-migration]: https://gitlab.com/diasporg/poduptime/blob/master/db/migrations/2.8.0-3.0.0.sql
[2.8.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.7.0-2.8.0.sql
[2.7.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.6.0-2.7.0.sql
[2.6.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.5.2-2.6.0.sql
[2.5.2-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.5.0-2.5.2.sql
[2.5.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.4.1-2.5.0.sql
[2.4.1-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.4.0-2.4.1.sql
[2.4.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.3.0-2.4.0.sql
[2.3.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.2.0-2.3.0.sql
[2.2.0-sql-migration]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/migrations/2.1.4-2.2.0.sql

[Unreleased]: https://gitlab.com/diasporg/poduptime/compare/master...develop
[3.9.0]: https://gitlab.com/diasporg/poduptime/compare/3.8.1...3.9.0
[3.8.1]: https://gitlab.com/diasporg/poduptime/compare/3.8.0...3.8.1
[3.8.0]: https://gitlab.com/diasporg/poduptime/compare/3.7.0...3.8.0
[3.7.0]: https://gitlab.com/diasporg/poduptime/compare/3.6.2...3.7.0
[3.6.0]: https://gitlab.com/diasporg/poduptime/compare/3.6.1...3.6.2
[3.6.0]: https://gitlab.com/diasporg/poduptime/compare/3.6.0...3.6.1
[3.6.0]: https://gitlab.com/diasporg/poduptime/compare/3.5.7...3.6.0
[3.5.7]: https://gitlab.com/diasporg/poduptime/compare/3.5.6...3.5.7
[3.5.6]: https://gitlab.com/diasporg/poduptime/compare/3.5.5...3.5.6
[3.5.5]: https://gitlab.com/diasporg/poduptime/compare/3.5.4...3.5.5
[3.5.4]: https://gitlab.com/diasporg/poduptime/compare/3.5.3...3.5.4
[3.5.3]: https://gitlab.com/diasporg/poduptime/compare/3.5.2...3.5.3
[3.5.1]: https://gitlab.com/diasporg/poduptime/compare/3.5.0...3.5.1
[3.5.0]: https://gitlab.com/diasporg/poduptime/compare/3.4.1...3.5.0
[3.4.1]: https://gitlab.com/diasporg/poduptime/compare/3.4.0...3.4.1
[3.4.0]: https://gitlab.com/diasporg/poduptime/compare/3.3.0...3.4.0
[3.3.0]: https://gitlab.com/diasporg/poduptime/compare/3.2.0...3.3.0
[3.2.0]: https://gitlab.com/diasporg/poduptime/compare/3.1.0...3.2.0
[3.1.0]: https://gitlab.com/diasporg/poduptime/compare/3.0.0...3.1.0
[3.0.0]: https://gitlab.com/diasporg/poduptime/compare/2.9.4...3.0.0
[2.9.2]: https://gitlab.com/diasporg/poduptime/compare/2.9.2...2.9.3
[2.9.2]: https://gitlab.com/diasporg/poduptime/compare/2.9.1...2.9.2
[2.9.1]: https://git.feneas.org/diasporg/Poduptime/compare/2.9.0...2.9.1
[2.9.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.8.2...2.9.0
[2.8.2]: https://git.feneas.org/diasporg/Poduptime/compare/2.8.1...2.8.2
[2.8.1]: https://git.feneas.org/diasporg/Poduptime/compare/2.8.0...2.8.1
[2.8.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.7.0...2.8.0
[2.7.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.6.4...2.7.0
[2.6.1]: https://git.feneas.org/diasporg/Poduptime/compare/2.6.0...2.6.1
[2.6.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.5.2...2.6.0
[2.5.2]: https://git.feneas.org/diasporg/Poduptime/compare/2.5.1...2.5.2
[2.5.1]: https://git.feneas.org/diasporg/Poduptime/compare/2.5.0...2.5.1
[2.5.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.4.2...2.5.0
[2.4.2]: https://git.feneas.org/diasporg/Poduptime/compare/2.4.1...2.4.2
[2.4.1]: https://git.feneas.org/diasporg/Poduptime/compare/2.4.0...2.4.1
[2.4.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.3.1...2.4.0
[2.3.1]: https://git.feneas.org/diasporg/Poduptime/compare/2.3.0...2.3.1
[2.3.0]: https://git.feneas.org/diasporg/Poduptime/compare/v2.2.0...2.3.0
[2.2.0]: https://git.feneas.org/diasporg/Poduptime/compare/2.1.3...v2.2.0

[Keep a Changelog]: https://keepachangelog.com/
[Semantic Versioning]: https://semver.org/
