<?php

/**
 * add a server from Async Task call
 */

declare(strict_types=1);

namespace Poduptime;

use Exception;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Task;
use Laminas\Validator\Hostname;

class AddServerTask extends Task
{
    public $output;
    public function __construct(
        protected string $domain
    ) {
    }

    public function configure()
    {
        require_once __DIR__ . '/../boot.php';
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $publickey = $_SERVER['DOMAIN'] . "-site-verification=" . bin2hex(random_bytes(9));
        $domain    = cleanDomain($this->domain);
        if (checkNodeinfo($domain)) {
            try {
                $p                 = R::dispense('pods');
                $p['domain']       = $domain;
                $p['date_updated'] = date('Y-m-d H:i:s');
                $p['publickey']    = $publickey;
                $this->output      = R::store($p);
                podLog('server added to database, record:' . $this->output, $domain);
            } catch (RedException) {
                $_SERVER['APP_DEBUG'] && podLog('dupe server not added database', $domain, 'error');
            }
        }
    }
}
