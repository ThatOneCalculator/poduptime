<?php

/**
 * Collection of functions that make remote calls to other servers.
 */

declare(strict_types=1);

use DCarbone\CURLHeaderExtractor;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Pool;
use GeoIp2\Database\Reader;
use DetectLanguage\DetectLanguage;
use DetectLanguage\DetectLanguageError;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use tomverran\Robot\RobotsTxt;

/**
 * Execute cURL request and return array of data.
 */
function curl(string $url, bool $follow = false, int $timeout = 30, string $token = null): array
{
    $chss = curl_init();

    if ($token) {
        curl_setopt($chss, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer $token",
        ]);
    }

    curl_setopt($chss, CURLOPT_URL, $url);
    curl_setopt($chss, CURLOPT_DNS_SERVERS, ($_SERVER['DNS_SERVER'] ?: '1.1.1.1') . ',' . ($_SERVER['DNS_SERVER_BACKUP'] ?: '1.0.0.1'));
    curl_setopt($chss, CURLOPT_CONNECTTIMEOUT, (int) $_SERVER['PROCESS_TIMEOUT'] ?: $timeout);
    curl_setopt($chss, CURLOPT_TIMEOUT, (int) $_SERVER['PROCESS_TIMEOUT'] ?: $timeout);
    curl_setopt($chss, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($chss, CURLOPT_CERTINFO, true);
    curl_setopt($chss, CURLOPT_ENCODING, '');
    curl_setopt($chss, CURLOPT_TCP_FASTOPEN, '1L');
    curl_setopt($chss, CURLOPT_MAXCONNECTS, (int) $_SERVER['PROCESS_LIMIT'] ?: 50);
    curl_setopt($chss, CURLOPT_SSL_FALSESTART, false);
    curl_setopt($chss, CURLOPT_MAXREDIRS, 5);
    curl_setopt($chss, CURLOPT_FOLLOWLOCATION, $follow);
    curl_setopt($chss, CURLOPT_USERAGENT, $_SERVER['USERAGENT']);
    curl_setopt($chss, CURLOPT_HEADER, true);

    $exec = curl_exec($chss);
    if (is_string($exec)) {
        list($headers, $body) = CURLHeaderExtractor::getHeaderAndBody($exec);
    } else {
        $headers = $exec;
        $body    = '';
    }

    $data = [
        'body'           => $body,
        'headers'        => $headers,
        'error'          => curl_error($chss),
        'info'           => curl_getinfo($chss, CURLINFO_CERTINFO),
        'code'           => curl_getinfo($chss, CURLINFO_RESPONSE_CODE),
        'conntime'       => curl_getinfo($chss, CURLINFO_CONNECT_TIME),
        'nstime'         => curl_getinfo($chss, CURLINFO_NAMELOOKUP_TIME),
        'downloadspeed'  => curl_getinfo($chss, CURLINFO_SPEED_DOWNLOAD_T),
        'totaltime'      => curl_getinfo($chss, CURLINFO_TOTAL_TIME)
    ];

    curl_close($chss);

    return $data;
}

/**
 * update all git data from list in config file
 */
function masterVersionCrawl(): bool
{
    $softwares = c('softwares');

    foreach ($softwares as $software => $details) {
        if (!empty($details['gittype'])) {
            if ($details['gittype'] === 'github') {
                $rjson = curl('https://' . $details['gitsite'] . '/repos/' . $details['repo'] . '/releases/latest');
                if ($rjson['code'] === 200) {
                    $releasejson = json_decode($rjson['body'] ?: '');
                    if ($details['devbranch']) {
                        $cjson = curl('https://' . $details['gitsite'] . '/repos/' . $details['repo'] . '/commits/' . $details['devbranch']);
                        $commitjson = json_decode($cjson['body'] ?: '');
                    }
                    if (isset($releasejson->tag_name) && $masterversion = $releasejson->tag_name ? str_replace('v', '', $releasejson->tag_name) : '') {
                        try {
                            $m = R::dispense('masterversions');
                            $m['software'] = $software;
                            $m['version'] = preg_replace('/-.*$/', '', $masterversion);
                            if (isset($releasejson->published_at) && $releasedate = $releasejson ? $releasejson->published_at : '') {
                                $m['releasedate'] = $releasedate;
                            }
                            if (isset($commitjson->commit->author->date) && $devlastcommit = $commitjson ? $commitjson->commit->author->date : '') {
                                $m['devlastcommit'] = $devlastcommit;
                            }
                            $id = R::store($m);
                            podLog('masterversion data stored' . $id, $software);
                        } catch (RedException $e) {
                            podLog('Error in SQL query ' . $e->getMessage(), $software, 'error');
                        }
                    }
                } else {
                    podLog('Error in masterversion lookup, error code: ' . $rjson['code'], $software, 'error');
                }
            } elseif ($details['gittype'] === 'gitlab') {
                $rjson = curl('https://' . $details['gitsite'] . '/api/v4/projects/' . $details['repo'] . '/repository/tags');
                if ($rjson['code'] === 200) {
                    $releasejson = json_decode($rjson['body'] ?: '');
                    if ($details['devbranch']) {
                        $cjson = curl('https://' . $details['gitsite'] . '/api/v4/projects/' . $details['repo'] . '/repository/commits/' . $details['devbranch']);
                        $commitjson = json_decode($cjson['body'] ?: '');
                    }
                    if (isset($releasejson[0]->name) && $masterversion = $releasejson[0]->name ? str_replace('v', '', $releasejson[0]->name) : '') {
                        try {
                            $m = R::dispense('masterversions');
                            $m['software'] = $software;
                            $m['version'] = preg_replace('/-.*$/', '', $masterversion);
                            if (isset($releasejson[0]->commit->created_at) && $releasedate = $releasejson[0] ? $releasejson[0]->commit->created_at : '') {
                                $m['releasedate'] = $releasedate;
                            }
                            if (isset($commitjson->created_at) && $devlastcommit = $commitjson ? $commitjson->created_at : '') {
                                $m['devlastcommit'] = $devlastcommit;
                            }
                            $id = R::store($m);
                            podLog('masterversion data stored' . $id, $software);
                        } catch (RedException $e) {
                            podLog('Error in SQL query ' . $e->getMessage(), $software, 'error');
                        }
                    }
                }
            } elseif ($details['gittype'] === 'gitea') {
                $rjson = curl('https://' . $details['gitsite'] . '/api/v1/repos/' . $details['repo'] . '/releases');
                if ($rjson['code'] === 200) {
                    $releasejson = json_decode($rjson['body'] ?: '');
                    if ($details['devbranch']) {
                        $cjson = curl('https://' . $details['gitsite'] . '/api/v1/repos/' . $details['repo'] . '/commits/' . $details['devbranch'] . '/status');
                        $commitjson = json_decode($cjson['body'] ?: '');
                    }
                    if (isset($releasejson[0]->tag_name) && $masterversion = $releasejson[0]->tag_name ? str_replace('v', '', $releasejson[0]->tag_name) : '') {
                        try {
                            $m = R::dispense('masterversions');
                            $m['software'] = $software;
                            $m['version'] = preg_replace('/-.*$/', '', $masterversion);
                            if (isset($releasejson[0]->created_at) && $releasedate = $releasejson[0] ? $releasejson[0]->created_at : '') {
                                $m['releasedate'] = $releasedate;
                            }
                            if (isset($commitjson->created_at) && $devlastcommit = $commitjson ? $commitjson->created_at : '') {
                                $m['devlastcommit'] = $devlastcommit;
                            }
                            $id = R::store($m);
                            podLog('masterversion data stored' . $id, $software);
                        } catch (RedException $e) {
                            podLog('Error in SQL query ' . $e->getMessage(), $software, 'error');
                        }
                    }
                } else {
                    podLog('Error in masterversion lookup, error code: ' . $rjson['code'], $software, 'error');
                }
            }
        }
    }
    return true;
}

/**
 * Helper to extract pods from cURL response.
 */
function extractPodsFromCurl(array $curl, ?string $field = null, ?string $column = null): array
{
    try {
        $pods = json_decode($curl['body'] ?: '', true, 512, JSON_THROW_ON_ERROR);
    } catch (JsonException) {
        return [];
    }

    if (!$field && !$column) {
        return $pods;
    }

    if ($field) {
        $pods = $pods[$field];
    }

    if ($column) {
        return array_column($pods, $column);
    }

    return $pods;
}

/**
 * Check for internet connection
 */
function isConnected(): bool
{
    return (bool) @fsockopen('icanhazip.com', 80);
}

/**
 *  See if blocked by robots file
 */
function robotAllowed(string $domain, string $path): bool
{
    try {
        $robotsTxt = new RobotsTxt(curl("https://$domain/robots.txt", true, (int) $_SERVER['PROCESS_TIMEOUT'] ?? 30)['body']);
        return $robotsTxt->isAllowed($_SERVER['USERAGENT'], "$path");
    } catch (InvalidArgumentException) {
        return false;
    }
}

/**
 * get IP datas
 */
function ipData($domain): array
{
    $dnsserver = $_SERVER['DNS_SERVER'] ?: '1.1.1.1';
    $dnsserver_backup = $_SERVER['DNS_SERVER_BACKUP'] ?: '1.0.0.1';

    $ipData = [
        'dnssec' => false,
    ];

    $lookups = [
        'ipv4' => 'A',
        'ipv6' => 'AAAA',
        'txt'  => 'TXT',
    ];

    foreach ($lookups as $key => $lookup) {
        try {
            @//todo dig can do idn but Process seems to fail out, this works for now
            $dig = new Process(['dig', "@$dnsserver", idn_to_ascii($domain), '+dnssec', $lookup]);
            $dig->setTimeout((int) $_SERVER['PROCESS_TIMEOUT'] ?: 30);
            $dig->mustRun();
            if (!$dig->isSuccessful()) {
                $dig = new Process(['dig', "@$dnsserver_backup", $domain, '+dnssec', $lookup]);
                $dig->setTimeout((int) $_SERVER['PROCESS_TIMEOUT'] ?: 30);
                $dig->mustRun();
            }
            $lookupData       = explode(PHP_EOL, trim($dig->getOutput()));
            $ipData['dnssec'] = $ipData['dnssec'] || preg_grep('/;; flags:.*ad.*;/', $lookupData);
            $lookupValues     = preg_grep('/\s+IN\s+' . $lookup . '\s+.*/', $lookupData);
            foreach ($lookupValues as $lookupValue) {
                preg_match('/' . $lookup . '\s(.*)/', $lookupValue, $match);
                $ipData[$key][] = $match[1];
            }
        } catch (ProcessFailedException $exception) {
            podLog('ipData lookup failed' . $exception->getMessage());
        }
    }

    return $ipData;
}

/**
 * get IP location datas
 */
function ipLocation($ip): array
{
    $validator = new Laminas\Validator\Ip();
    if (!$validator->isValid($ip)) {
        return [];
    }

    try {
        $reader = new Reader($_SERVER['GEOLITE_CITY']);
        $geo = $reader->city($ip);
    } catch (InvalidDatabaseException | AddressNotFoundException $e) {
        return [];
    }
    //could be improved using the languages that come back, different for every IP
    $metalocation = $geo->country->names['en'] ?? null;
    $metalocation .= $geo->city->names['en'] ?? null;
    $metalocation .= $geo->continent->names['en'] ?? null;
    $metalocation .= $geo->subdivisions['0']->names['en'] ?? null;

    return [
        'countryname' => $geo->country->name ?? null,
        'country' => $geo->country->isoCode ?? null,
        'city' => $geo->city->name ?? null,
        'state' => $geo->mostSpecificSubdivision->name ?? null,
        'lat' => $geo->location->latitude ?? null,
        'long' => $geo->location->longitude ?? null,
        'zipcode' => $geo->postal->code ?? null,
        'metalocation' => $metalocation
    ];
}

/**
 * fetch and parse nodeinfo
 */
function nodeInfo($domain, $timeout = 30): ?array
{
    $nodeinfo_meta = curl("https://$domain/.well-known/nodeinfo", false, (int) $_SERVER['PROCESS_TIMEOUT'] ?? $timeout);

    $nodeinfourl = "https://$domain/nodeinfo/1.0";

    if (array_key_exists('links', json_decode($nodeinfo_meta['body'], true) ?? [])) {
        if ($info = json_decode($nodeinfo_meta['body'] ?: '', true)) {
            if (count($info['links'], COUNT_RECURSIVE) === 2) {
                $nodeinfourl = $info['links']['href'];
            } elseif (count($info['links'], COUNT_RECURSIVE) === 4) {
                $nodeinfourl = $info['links'][1]['href'];
            } else {
                $nodeinfourl = max($info['links'])['href'];
            }
        }
    }
    if (parse_url($nodeinfourl, PHP_URL_HOST) == $domain or parse_url($nodeinfourl, PHP_URL_HOST) == idn_to_ascii($domain)) {
        $nodeinfo = curl($nodeinfourl, false, (int)$_SERVER['PROCESS_TIMEOUT'] ?? $timeout);
        return [
            'nodeinfourl' => $nodeinfourl,
            'body' => $nodeinfo['body'],
            'sslerror' => $nodeinfo['error'],
            'info' => $nodeinfo['info'],
            'httpcode' => $nodeinfo['code'],
            'conntime' => $nodeinfo['conntime'],
            'downloadspeed' => $nodeinfo['downloadspeed'],
            'totaltime' => $nodeinfo['totaltime'],
            'header' => $nodeinfo['headers'],
            'nstime' => $nodeinfo['nstime'],
            'sslexpire' => $nodeinfo['info'][0]['Expire date'] ?? null
        ];
    } else {
        return null;
    }
}

/**
 * Get a language snippet from a given URL.
 */
function getWebsiteSnippetFromUrl(string $url): ?array
{
    $curl = curl($url, true, (int) $_SERVER['PROCESS_TIMEOUT'] ?? 30);

    if (!$curl['body']) {
        return null;
    }

    libxml_use_internal_errors(true);

    $d = new DOMDocument();
    $d->loadHTML($curl['body']);

    $title       =  trim($d->getElementsByTagName('title')->item(0)->textContent, " \n\r\t\v\0") ?? '';
    $body        = $title;
    $description = '';
    $image       = '';

    foreach ($d->getElementsByTagName('meta') as $meta) {
        if (strtolower($meta->getAttribute('name')) === 'description') {
            $body        .= ' ' . $meta->getAttribute('content');
            $description = $meta->getAttribute('content');
        }
        if (strtolower($meta->getAttribute('property')) === 'og:image') {
            $image = $meta->getAttribute('content');
        }
    }

    return array_map('strip_tags', compact('body', 'title', 'description', 'image'));
}

/**
 * Detect the language of the given text snippet.
 */
function detectWebsiteLanguageFromSnippet(string $snippet): ?string
{
    if (!$snippet) {
        return null;
    }

    try {
        return DetectLanguage::simpleDetect($snippet);
    } catch (DetectLanguageError $e) {
        podLog('Error in DetectLanguage: ' . $e->getMessage());
        return null;
    }
}
