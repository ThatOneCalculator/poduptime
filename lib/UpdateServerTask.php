<?php

/**
 * update a server from Async Task call
 */

declare(strict_types=1);

namespace Poduptime;

use Carbon\Carbon;
use DateTime;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Task;

class UpdateServerTask extends Task
{
    public function __construct(
        protected string $domain
    ) {
    }

    public function configure()
    {
        require_once __DIR__ . '/../boot.php';
    }

    public function run()
    {
        $debug  = $_SERVER['APP_DEBUG'] ?? false;
        $domain = $this->domain;
        try {
            $pod = R::findOne('pods', 'domain = ?', [$domain]);
        } catch (RedException $e) {
            die('Error in SQL query: ' . $e->getMessage());
        }

        if (!$pod) {
            // @todo Better error handling!
            return "Pod not found for: $domain";
        }

        $score = (int) $pod['score'];
        $softwares = c('softwares');
        foreach ($softwares as $software => $details) {
            if ($software === $pod['softwarename']) {
                $pod['pp'] = (empty($pod['pp']) && !empty($details['pp'])) ? 'https://' . $domain . $details['pp'] : $pod['pp'];
                $pod['support'] = (empty($pod['support']) && !empty($details['support'])) ? 'https://' . $domain . $details['support'] : $pod['support'];
                $pod['terms'] = (empty($pod['terms']) && !empty($details['terms'])) ? 'https://' . $domain . $details['terms'] : $pod['terms'];
            }
        }

        $nodeinfo = nodeInfo($domain, (int) ($_SERVER['PROCESS_TIMEOUT'] ?? 30));
        if ($nodeinfo) {
            $debug && podLog('Nodeinfo URL ' . $nodeinfo['nodeinfourl'], $domain, 'debug');
            $latency = $nodeinfo['conntime'] + $nodeinfo['nstime'];
            $sslerror = $pod['sslvalid'] = $nodeinfo['sslerror'] ?? null;
            $sslexpire = $pod['sslexpire'] = $nodeinfo['sslexpire'] ?? null;
            $pod['onion'] = parse_url($nodeinfo['header'][0]['onion-location'][0] ?? '', PHP_URL_HOST);
            $pod['i2p'] = parse_url($nodeinfo['header'][0]['x-i2p-location'][0] ?? '', PHP_URL_HOST);
            $pod['servertype'] = $nodeinfo['header'][0]['server'][0] ?? '';
            $debug && podLog('Nodeinfo Server Type ' . $pod['servertype'], $domain, 'debug');
            $debug && podLog('Nodeinfo error ' . $nodeinfo['sslerror'], $domain, 'debug');
            $debug && podLog('Nodeinfo service HTTP response code ' . $nodeinfo['httpcode'], $domain, 'debug');
            $debug && podLog('Cert expire date ' . $sslexpire, $domain, 'debug');
            $debug && podLog('Latency ms ' . $latency, $domain, 'debug');
            $debug && podLog('Download Speed in kbps ' . $nodeinfo['downloadspeed'] * 0.008, $domain, 'debug');

            $jsonssl = json_decode($nodeinfo['body'] ?? null);
        }

        if (isset($jsonssl) && $nodeinfo['httpcode'] == 200) {
            $fullversion = $pod['fullversion'] = $jsonssl->software->version ?? '999.999.999';
            // @todo this 999 thing is a bad hack, can show in the UI and makes no sense
            if ($fullversion === '999.999.999') {
                $score -= 2;
            }
            preg_match_all('((?:\d(.|-)?)+(\.)\d+\.*)', $fullversion, $sversion);
            $shortversion = $pod['shortversion'] = $sversion[0][0] ?? '0.0.0.0';
            $pod['signup'] = ($jsonssl->openRegistrations ?? false) === true;
            $softwarename = $pod['softwarename'] = strtolower($jsonssl->software->name ?? 'unknown');
            $name = $pod['name'] = $jsonssl->metadata->nodeName ?? $pod['softwarename'] ?? 'unknown';
            $pod['total_users'] = $jsonssl->usage->users->total ?? $pod['total_users'] ?? 0;
            $pod['active_users_halfyear'] = $jsonssl->usage->users->activeHalfyear ?? $pod['active_users_halfyear'] ?? 0;
            $pod['active_users_monthly'] = $jsonssl->usage->users->activeMonth ?? $pod['active_users_monthly'] ?? 0;
            $pod['local_posts'] = $jsonssl->usage->localPosts ?? $pod['local_posts'] ?? 0;
            $pod['comment_counts'] = $jsonssl->usage->localComments ?? $pod['comment_counts'] ?? 0;
            $pod['owner'] = $jsonssl->metadata->adminAccount ?? $pod['owner'];
            $pod['camo'] = json_encode($jsonssl->metadata->camo ?? '') ?? $pod['camo'] ?? '';
            $pod['service_xmpp'] = ($jsonssl->metadata->xmppChat ?? false) === true;
            $pod['services'] = json_encode($jsonssl->services->outbound ?? null) ?? $pod['services'] ?? null;
            $pod['protocols'] = json_encode($jsonssl->protocols->outbound ?? $jsonssl->protocols) ?? $pod['protocols'] ?? null;
            $score += 1;

            writeCheck([$domain, true, null, $latency, $pod['total_users'], $pod['active_users_halfyear'], $pod['active_users_monthly'], $pod['local_posts'], $pod['comment_counts'], $shortversion, $fullversion]);

            try {
                $masterdata = R::getRow('
                SELECT version, devlastcommit, releasedate, date_checked
                FROM masterversions
                WHERE software = ?
                ORDER BY id
                DESC LIMIT 1
            ', [$softwarename]);
            } catch (RedException $e) {
                podLog('Error in SQL query ' . $e->getMessage(), $domain, 'error');
            }

            try {
                $versionhistory = R::getAll('
                SELECT distinct on (shortversion) shortversion, date_checked 
                FROM checks 
                WHERE domain = ? 
                AND shortversion = ?
                ORDER BY shortversion, date_checked
        ', [$domain, $shortversion]);
            } catch (RedException $e) {
                podLog('Error in SQL query ' . $e->getMessage(), $domain, 'error');
            }

            $totalchecks = R::count('checks', ' domain = ? ', [ $domain ]);
            $masterversion = $pod['masterversion'] = $masterdata['version'] ?? '0.0.0.0';
            $debug && podLog('Version code ' . $shortversion, $domain, 'debug');
            $debug && podLog('Masterversion ' . $masterversion, $domain, 'debug');

            try {
                $dayssinceupdate = (int) date_diff(new DateTime($versionhistory[0]['date_checked']), new DateTime())->format('%a');
            } catch (Exception $e) {
                podLog('Error in DateTime ' . $e->getMessage(), $domain, 'error');
            }

            if (version_compare($shortversion, $masterversion, '>')) {
                $debug && podLog('Code is ahead of repo version, days since last updated server code ' . $dayssinceupdate, $domain, 'debug');
                if ($dayssinceupdate > ((int) $_SERVER['MIN_STALE_SCORE'] ?: 1000)) {
                    $score -= 2;
                }
            } elseif (version_compare($shortversion, $masterversion, '<')) {
                $debug && podLog('Code is behind of repo version, days since last updated server code ' . $dayssinceupdate, $domain, 'debug');
                if ($dayssinceupdate > ((int) $_SERVER['MIN_PRE_SCORE'] ?: 1000)) {
                    $score -= 2;
                }
            }
            $ipdata = ipData($domain);
            $pod['ip'] = $ipdata['ipv4'][0] ?? null;
            $pod['dnssec'] = $ipdata['dnssec'];
            $pod['ipv6'] = $ipdata['ipv6'][0] ?? null;
            if ($pod['ip']) {
                $iplocaton = ipLocation($pod['ip']);
            } elseif ($pod['ipv6']) {
                $iplocaton = ipLocation($pod['ipv6']);
            }
            if ($pod['servertype'] == 'cloudflare') {
                $pod['countryname'] = 'Private';
                $pod['country'] = 'CF';
                $pod['city'] = 'Private';
                $pod['state'] = 'Private';
                $pod['lat'] = null;
                $pod['long'] = null;
                $pod['zipcode'] = null;
                $pod['metalocation'] = 'Hidden Behind CloudFlare';
            } else {
                $pod['countryname'] = $iplocaton['countryname'] ?? null;
                $pod['country'] = $iplocaton['country'] ?? null;
                $pod['city'] = $iplocaton['city'] ?? null;
                $pod['state'] = $iplocaton['state'] ?? null;
                $pod['lat'] = $iplocaton['lat'] ?? null;
                $pod['long'] = $iplocaton['long'] ?? null;
                $pod['zipcode'] = $iplocaton['zipcode'] ?? null;
                $metalocation = $pod['metalocation'] = $iplocaton['metalocation'] ?? null;
            }
            try {
                $diff = (new DateTime())->diff(new DateTime($pod['date_created']));
            } catch (Exception $e) {
                podLog('Error in Datetime ' . $e->getMessage(), $domain, 'error');
            }
            $pod['monthsmonitored'] = $diff->m + ($diff->y * 12);
            $pod['daysmonitored'] = $diff->days;
            $debug && podLog('Location meta ' . $metalocation, $domain, 'debug');
            $debug && podLog('IPv4 ' . $pod['ip'], $domain, 'debug');
            $debug && podLog('IPv6 ' . $pod['ipv6'], $domain, 'debug');
            $debug && podLog('Signup Open ' . $pod['signup'], $domain, 'debug');

            $langhours = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('languages_updated', 'date_created'))->diffInHours();
            $robotallowed = robotAllowed("$domain", "/");
            if (($langhours > ($_SERVER['LANGUAGE_REFRESH'] ?? 24) && $robotallowed) || $totalchecks < 3) {
                $html_snippet = getWebsiteSnippetFromUrl("https://$domain/");
                if ($html_snippet['body']) {
                    $pod['detectedlanguage'] = detectWebsiteLanguageFromSnippet($html_snippet['body']) ?? $pod['detectedlanguage'];
                    $pod['metatitle'] = $html_snippet['title'] ?? $pod['metatitle'] ?? '';
                    $pod['metadescription'] = $html_snippet['description'] ?? $pod['metadescription'] ?? '';
                    $pod['metaimage'] = $html_snippet['image'] ?? $pod['metaimage'] ?? '';
                }
                $debug && podLog('Detected Language ' . $pod['detectedlanguage'], $domain, 'debug');
                $debug && podLog('Meta Tag Title ' . $pod['metatitle'], $domain, 'debug');
                $debug && podLog('Meta Tag Description ' . $pod['metadescription'], $domain, 'debug');
                $debug && podLog('Meta Image ' . $pod['metaimage'], $domain, 'debug');
            } elseif (!$robotallowed) {
                $pod['metatitle'] = $pod['name'] ?? $name ?? 'unknown';
                $pod['metadescription'] = $pod['softwarename'] ?? $softwarename ?? 'unknown';
            }

            $status = PodStatus::UP;
        }

        if ($nodeinfo['httpcode'] !== 200 || !isset($jsonssl)) {
            $debug && podLog('Can not connect to server', $domain, 'debug');
            if ($nodeinfo['httpcode'] > 0) {
                writeCheck([$domain, false, 'HTTP response code: ' . $nodeinfo['httpcode'], $latency]);
            } elseif ($sslerror) {
                writeCheck([$domain, false, 'Error: ' . $sslerror, $latency]);
            }
            $score -= 2;
            $status = PodStatus::DOWN;
        }

        try {
            $checks = R::getRow('
            SELECT
                round(avg(latency) * 1000) AS latency,
                round(avg(online::INT) * 100, 2) AS online
            FROM checks
            WHERE domain = ?
        ', [$domain]);
            $pod['latency'] = $checks['latency'] ?? 0;
            $pod['uptime_alltime'] = $checks['online'] ?? null;
        } catch (RedException $e) {
            podLog('Error in SQL query ' . $e->getMessage(), $domain, 'error');
        }
        $debug && podLog('Uptime ' . $pod['uptime_alltime'], $domain, 'debug');
        if ($score == ($pod['podmin_notify_level'] - 1) && $pod['podmin_notify'] && $_SERVER['APP_ENV'] == 'production' && (int) $pod['score'] == $pod['podmin_notify_level']) {
            $message = 'Notice for ' . $domain . '. Your score is ' . $score . ' and your server will fall off the list soon. You can find details in the edit server function.';
            sendEmail($pod['email'], 'Monitoring Notice from', $message);
        }
        $weightedscore = ($pod['uptime_alltime'] + $score + $pod['monthsmonitored'] - (10 - $pod['weight'])) / 2;

        if ($score > 100) {
            $score = 100;
        }
        if ($score < 1) {
            $weightedscore = 0;
        }
        if ($score < ((int) $_SERVER['MIN_SCORE'] ?? -100)) {
            $status = PodStatus::SYSTEM_DELETED;
        }

        $debug && podLog('Score ' . $score, $domain, 'debug');
        $debug && podLog('Status ' . $status, $domain, 'debug');
        $debug && podLog('Weighted Score ' . $weightedscore, $domain, 'debug');
        try {
            $pod['weightedscore'] = $weightedscore;
            $pod['status'] = $status;
            $pod['date_laststats'] = date('Y-m-d H:i:s');
            $pod['score'] = $score;
            $id = R::store($pod);
            podLog('server data stored, record:' . $id, $domain);
        } catch (RedException $e) {
            podLog('Error in SQL query ' . $e->getMessage(), $domain, 'error');
        }
    }
}
