---
base:
  general:
    yes: 'Yes'
    no: 'No'
    up: Up
    down: Down
    close: Close
    next: Next
    back: Back
    server: Server
    servers: Servers
    submit: Submit
    save: Save
    delete: Delete
    pause: Pause
    unpause: Unpause
    error: Error
    date: Date
  navs:
    list: List
    map: Map
    stats: Stats
    software: Software
    all: all
    search: Search
    add: Add
    edit: Edit
    source: Source
    api: API
    updated: Updated
    status: Status
    translation: Translate
    terms: Terms
  strings:
    about: Fediverse Observer checks all servers in the fediverse and gives you an easy way to find a home using a map or list
    list:
      loading: Loading data, this takes a few seconds..
      loadingall: Loading data, this can be a lot on your browser, filter to a software on the menu to speed things up...
      userstats: User stats for %(domain)
      actionstats: Action stats for %(domain)
      uptimestats: Uptime stats for %(domain)
      cookienotice: This is the last server you visited from this site
      devcode: This server runs pre release development code
      unknowncode: This server runs unknown code
      productioncode: This server runs production code
      navs:
        auto: Auto Pick
        wizard: Use Wizard
        all: All Columns
        map: Use Map
        basic: Basic Columns
        select: Select Columns
        reset: Reset Filters
      columns:
        server: Server
        serverdesc: A server is a site for you to set up your account
        name: Name
        namedesc: Defined name of this server
        version: Version
        versiondesc: Version of software this server runs
        software: Software
        softwaredesc: Type of fediverse software this server runs
        uptime: Uptime
        uptimedesc: Percent of the time the server is online
        latency: Latency
        latencydesc: Average connection latency in ms from %(hostlocation)
        signups: Signups
        signupsdesc: Does this server allow new users
        users: Users
        usersdesc: Number of total users on this server
        users6: 6m
        users6desc: Number of users active last 6 months on this server
        users1: 1m
        users1desc: Number of users active last 1 month on this server
        posts: Posts
        postsdesc: Number of total posts on this server
        comments: Comments
        commentsdesc: Number of total comments on this server
        months: Months
        monthsdesc: How many months have we been watching this server
        score: Score
        scoredesc: System Score on a 100 point scale
        status: Status
        statusdesc: System Status
        country: Country
        countrydesc: Server country, based on IP Geolocation
        city: City
        citydesc: Server city, based on IP Geolocation
        state: State
        statedesc: Server state, based on IP Geolocation
        language: Language
        languagedesc: Server language auto-detected from their main page data
    map:
      title: Map view of
      tip: Use the software menu selector to limit this data.
    search:
      software: Software
      language: Language
      location: Location
      open: Open for Signups
      results: '%(number) results for %(searchterm)'
    stats:
      title: Entire Fediverse
      users: Users by Software
      serverss: Servers by Software
      serversc: Servers by Country
      statsfor: Stats for
      average: Average
      growth: Network Total Users Per Month
      growthactive: Network Active Users Per Month
      serversper: Servers Online Per Month
      commentsper: Comments Per Month
      postsper: Posts Per Month
      tip: Pie charts are top 10. All graphs are averaged monthly data. Use the software selector on menu to change these charts to just one software vs all.
    singlepage:
      uptime: Uptime & Speed
      userstats: User Stats
      actionstats: Action Stats
      clicksout: Clicks Out
      opensignup: This server is allowing new users to sign up
      closedsignup: This server is not accepting new users
      lastchecked: Server last checked
      version: This server %(domain) runs %(software) software, version %(version)
      status: This server has been monitored since %(daysmonitored) and has a score of %(score) out of %(maxscore)
      language: Detected language of this server is %(language)
      location: Server looks to be located in %(location)
      nolocation: Could not detect location of this Server
      notfound: Fediverse server not found
      addit: You can always add it
      private: This server uses a CDN to block it's actual location, you should investigate where the actual server is located before using
      about: About
      charts: Charts
      data: Data
      errors: Errors
      query: Querying API
      deleted: Server has been deleted
      findother: We can help you find a new %(software) server
    status:
      current: System status is
      red: Red
      green: Green
      status: and currently
      idle: Idle
      running: Running
      last: Last Update scanned
      update: Last Update was
      updatetook: Last Update took
      language: Last Language check was
      backup: Last Data Backup was
      masterversion: Last Masterversion update from git repos was
      crawl: Last Fediverse crawl was
      monthly: Last Monthly stats table update was
      poduptimeversion: Software Version
      branch: Branch
      first: This site found its first server
      andtotal: and discovered a total of
    wizard:
      title: Fediverse Wizard
      about: This wizard will help you get started to find a home on the Fediverse
      recommend: Software we recommend
      country: What country do you want the server in?
      countrywhy: A country can dictate site speed and privacy rules for your new home
      countrynone: Country does not have a large user base, click next
      size: How big of a server to you want to be on?
      sizenote: Remember all servers talk to another, so biggest is not always best.
      usersmin: Users Minimum
      suggest: Suggest a Server
      wizardfinal: This will now add filters to the table of data for you.<br>You will be able to change these filters on the top row of each column or use this wizard again to help you.
      wizardnote: This will take your browser a few seconds to update to servers from these results
welcome:
  main:
    find: Let's find you a fediverse home!
    suggested: Use a suggested server close to you
    picked: '%(domain) is a %(software) server in %(location)'
    nopicked: No good servers near %(location) for %(software) were found, use list or map to find more
    keeplooking: Or, keep looking
    first: First, Filter sites by software
    second: Then, Filter sites by view
    map: Use a <b class="fw-bold">map</b> to find a server close to you. <br>This will make things faster for you!
    list: Use a <b class="fw-bold">list</b> of servers to find a home. <br>You can filter and sort on the top of the list.
softwares:
  diaspora: Diaspora is a privacy-aware, distributed, open source social network
  friendica: Friendica is a decentralised communications platform that integrates social communication
  hubzilla: Hubzilla is a powerful platform for creating interconnected websites featuring a decentralized identity
  pleroma: Pleroma is a free, federated social networking server built on open protocols
  socialhome: Socialhome is best described as a federated personal profile with social networking functionality
  social-relay: A relay system for the federation not for end use
  writefreely: WriteFreely is free and open source software for building a writing space on the web
  ganggo: GangGo is a decentralized social network written in GoLang
  funkwhale: Funkwhale is a community-driven project that lets you listen and share music and audio within a decentralized, open network
  osada: Osada is a conversational style macroblogging network powered by hyper-drive social engine, supporting ActivityPub and Zot6 protocols
  mastodon: Mastodon is an open source decentralized social network - by the people for the people
  pixelfed: Pixelfed is a free and ethical photo sharing platform, powered by ActivityPub federation
  wordpress: Wordpress is open source software which you can use to easily create a beautiful website, blog, or app
  misskey: Misskey is a decentralized microblogging platform born on Earth
  speechmore: Speechmore is the social network that leaves you in control of your data and gives you the freedom to say whatever you have to
  peertube: PeerTube is a free and open-source, decentralized, federated video platform powered by ActivityPub and WebTorrent
  plume: Plume is a federated blogging application
  rustodon: Rustodon is an Mastodon-compatible federated social microblogging server
  microblogpub: Microblogpub is a self-hosted, single-user, ActivityPub powered microblog
  mobilizon: Mobilizon is a tool designed to create platforms for managing communities and events
  lemmy: Lemmy is an open-source, easily self-hostable link aggregator that you can use to share
  gnusocial: GNU social is the eldest free social networking platform for public and private communications used in federated social networks
  ecko: Ecko is a community-driven fork of Mastodons social network software. The idea for the fork is to optimize toward community, that is making it as easy as possible to contribute
  bookwyrm: BookWyrm is a platform for social reading! You can use it to track what youre reading, review books, and follow your friends
  akkoma: a smallish microblogging platform, aka the cooler pleroma
  calckey: Calckey is based off of Misskey, a powerful microblogging server on ActivityPub with features such as emoji reactions, a customizable web ui, rich chatting, and much more!
  drupal: With robust content management tools, sophisticated APIs for multichannel publishing, and a track record of continuous innovation—Drupal is the best digital experience platform(DXP) on the web
  epicyon: Epicyon is a fediverse server suitable for self-hosting a small number of accounts on low power systems
  foundkey: FoundKey is a free and open source microblogging server compatible with ActivityPub. Forked from Misskey, FoundKey improves on maintainability and behaviour, while also bringing in useful features
  gancio: a shared agenda for local communities (with activitypub support)
  ktistec: Ktiste is an ActivityPub server. It is intended for individual users
  owncast: Owncast is an open source, self-hosted, decentralized, single user live video streaming and chat server for running your own live streams similar in style to the large mainstream options
admin:
  add: Want your server listed? Or a server you use? Or to claim a listed server?
  domain: Server Domain
  domainnote: The base domain name of your server (without trailing slash).
  email: Your Email (optional)
  emailnote: We'll never share your email with anyone else.
  nodomain: no server domain given
  dupeserver: Server already exists and is registered to an owner, use the edit function to modify
  dnsrecord: Server already exists, you can claim this domain by adding a DNS TXT record that states
  dnsnote: Refresh this page after updating your DNS records to be able to add your email address to this server
  emailsuccess: Email added to domain
  emailmissing: Go back and enter the email you want to use on the form
  addemailsubject: New Server Added to
  addemailbody: New server https://%(domain) added to database. New servers start with a score of 50, give it some time to be checked and show up.
  addsuccess: Data successfully inserted! Your server will be checked and live on the list in a few hours!
  error: Could not validate your server, check your setup!<br>Take a look at your <a href="//%(domain)/.well-known/nodeinfo">nodeinfo</a>
  edit: Want to update your server?
  notoken: no token given
  noserver: no server domain given
  badtoken: bad token
  domainnotfound: domain not found
  mismatch: token mismatch
  expired: token expired
  deleted: server deleted
  paused: server paused
  unpaused: server unpaused
  outofrange: 10 is max weight
  editemailsubject: Edit notice from
  editemailbody: Data for %(domain) updated.
  editsuccess: Data saved. Will go into effect on next update
  authorized: Authorized to edit %(domain) for %(hours)
  emailrequired: Email
  termsurl: URL to terms
  ppurl: URL to Privacy Policy
  supporturl: 'URL to Support(use mailto: if email address)'
  statement: Admin Statement (500 character limit)
  weight: Weight
  weightnote: This lets you weight your server lower on the list if you have too much traffic coming in, 10 is the norm use lower to move down the list
  notify: Notify if server falls off the list?
  notifylevel: Notify when your score falls to
  status: Your server status is currently
  delete: Stop checking and Stop showing your server. Can be re-enabled later.
  pause: Stop checking your server. Can be re-enabled later.
  unpause: Unpause/Undelete - Start checking and Start showing your server.
  conerrors: Last 20 connection errors from your server
  tokenemailsubject: Edit Key for
  tokenemailbody: This link %(expires)
  tokensuccess: Link send to registered email
  register: This server does not have an email on file, use the <a href="/podmin">add a server link</a> to register your server.

