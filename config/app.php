<?php

require_once __DIR__ . '/../boot.php';

/**
 * Config for Poduptime menus.
 */

return [
    'pod-list-providers' => [
        [
            'curl'   => fn() => curl('https://the-federation.info/pods.json', false, 45),
            'field'  => 'pods',
            'column' => 'host'
        ],
        [
            'curl'   => fn() => curl('https://diasp.org/pods.json', false, 45),
            'column' => 'host'
        ],
        [
            'curl' => fn() => curl('https://fedidb.org/api/v0/network/instances', false, 45)
        ],
        [
            'curl' => fn() => curl('https://nodes.fediverse.party/nodes.json', false, 45)
        ],
    ],

    //softwares and git repos we support
    'softwares'   =>  [
        'diaspora'     => [
            'repo' => 'diaspora/diaspora',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'develop',
            'repository' => 'https://github.com/diaspora/diaspora',
            'info' => 'https://fediverse.party/en/diaspora',
            'wizard' => '1',
            'welcome' => '1',
            'protocol' => '1',
            'service' => '1',
            'terms' => '/terms',
            'pp' => '/terms#privacy',
            'support' => '',
            'text' => 'diaspora',
            'href' => 'diaspora',
            'faclass' => 'fa-diaspora',
            'title' => 'Publish to diaspora*'
            ],
        'akkoma'    => [
            'repo' => 'AkkomaGang/akkoma',
            'gitsite' => 'akkoma.dev',
            'gittype' => 'gitea',
            'devbranch' => 'develop',
            'repository' => 'https://akkoma.dev/AkkomaGang/akkoma/',
            'info' => 'https://akkoma.social/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/tos',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'akkoma',
            'href' => 'akkoma',
            'faclass' => 'fa-akkoma',
            'title' => 'Publish to akkoma'
        ],
        'calckey'    => [
            'repo' => 'thatonecalculator/calckey',
            'gitsite' => 'codeberg.org',
            'gittype' => 'gitea',
            'devbranch' => 'develop',
            'repository' => 'https://codeberg.org/thatonecalculator/calckey',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'calckey',
            'href' => 'calckey',
            'faclass' => 'fa-calckey',
            'title' => 'Publish to calckey'
        ],
        'drupal'    => [
            'repo' => 'project%2drupal',
            'gitsite' => 'git.drupalcode.org',
            'gittype' => 'gitlab',
            'devbranch' => 'develop',
            'repository' => 'https://git.drupalcode.org/project/drupal',
            'info' => 'https://www.drupal.org/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'drupal',
            'href' => 'drupal',
            'faclass' => 'fa-drupal',
            'title' => 'Publish to drupal'
        ],
        'epicyon'    => [
            'repo' => 'bashrc2/epicyon',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'main',
            'repository' => 'https://github.com/bashrc2/epicyon',
            'info' => 'https://epicyon.net/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'epicyon',
            'href' => 'epicyon',
            'faclass' => 'fa-epicyon',
            'title' => 'Publish to epicyon'
        ],
        'foundkey'    => [
            'repo' => 'FoundKeyGang/FoundKey',
            'gitsite' => 'akkoma.dev',
            'gittype' => 'gitea',
            'devbranch' => 'main',
            'repository' => 'https://akkoma.dev/FoundKeyGang/FoundKey',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'foundkey',
            'href' => 'foundkey',
            'faclass' => 'fa-foundkey',
            'title' => 'Publish to foundkey'
        ],
        'gancio'    => [
            'repo' => 'les%2gancio',
            'gitsite' => 'framagit.org',
            'gittype' => 'gitlab',
            'devbranch' => 'dev',
            'repository' => 'https://framagit.org/les/gancio',
            'info' => 'https://gancio.org/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'gancio',
            'href' => 'gancio',
            'faclass' => 'fa-gancio',
            'title' => 'Publish to gancio'
        ],
        'ktistec'    => [
            'repo' => 'toddsundsted/ktistec',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/toddsundsted/ktistec',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'ktistec',
            'href' => 'ktistec',
            'faclass' => 'fa-ktistec',
            'title' => 'Publish to ktistec'
        ],
        'owncast'    => [
            'repo' => 'owncast/owncast',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'develop',
            'repository' => 'https://github.com/owncast/owncast',
            'info' => 'https://owncast.online/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/terms',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'owncast',
            'href' => 'owncast',
            'faclass' => 'fa-owncast',
            'title' => 'Publish to owncast'
        ],
        'friendica'    => [
            'repo' => 'friendica/friendica',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'develop',
            'repository' => 'https://github.com/friendica/friendica',
            'info' => 'https://fediverse.party/en/friendica',
            'wizard' => '1',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/tos',
            'pp' => '/tos',
            'support' => '/help',
            'text' => 'friendica',
            'href' => 'friendica',
            'faclass' => 'fa-friendica',
            'title' => 'Publish to friendica'
        ],
        'hubzilla'     => [
            'repo' => 'hubzilla%2fcore',
            'gitsite' => 'framagit.org',
            'gittype' => 'gitlab',
            'devbranch' => 'dev',
            'repository' => '',
            'info' => 'https://fediverse.party/en/hubzilla',
            'wizard' => '1',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/help/TermsOfService',
            'pp' => '/help/TermsOfService',
            'support' => '/help',
            'text' => 'hubzilla',
            'href' => 'hubzilla',
            'faclass' => 'fa-hubzilla',
            'title' => 'Publish to Hubzilla'
        ],
        'pleroma'      => [
            'repo' => 'pleroma%2fpleroma',
            'gitsite' => 'git.pleroma.social',
            'gittype' => 'gitlab',
            'devbranch' => 'develop',
            'repository' => '',
            'info' => 'https://fediverse.party/en/pleroma',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/about/tos',
            'pp' => '/about/privacy',
            'support' => '/about',
            'text' => 'pleroma',
            'href' => 'pleroma',
            'faclass' => 'fa-pleroma',
            'title' => 'Publish to'
        ],
        'socialhome'   => [
            'repo' => 'jaywink/socialhome',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/jaywink/socialhome',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/terms',
            'pp' => '/privacy',
            'support' => '',
            'text' => 'socialhome',
            'href' => 'socialhome',
            'faclass' => 'fa-social-home',
            'title' => 'Publish to'
        ],
        'writefreely'  => [
            'repo' => 'writefreely/writefreely',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'develop',
            'repository' => 'https://github.com/writeas/writefreely',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '/privacy',
            'support' => '/about',
            'text' => 'writefreely',
            'href' => 'writefreely',
            'faclass' => 'fa-writefreely',
            'title' => 'Publish to'
        ],
        'funkwhale'    => [
            'repo' => 'funkwhale%2ffunkwhale',
            'gitsite' => 'dev.funkwhale.audio',
            'gittype' => 'gitlab',
            'devbranch' => 'develop',
            'repository' => '',
            'info' => 'https://fediverse.party/en/funkwhale',
            'wizard' => '0',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/about',
            'pp' => '/about',
            'support' => '/about',
            'text' => 'funkwhale',
            'href' => 'funkwhale',
            'faclass' => 'fa-funkwhale',
            'title' => 'Publish to funkwhale'
        ],
        'osada'        => [
            'repo' => '',
            'gitsite' => '',
            'gittype' => '',
            'devbranch' => '',
            'repository' => '',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'osada',
            'href' => 'osada',
            'faclass' => 'fa-osada',
            'title' => 'Publish to osada'
        ],
        'mastodon'    => [
            'repo' => 'mastodon/mastodon',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/mastodon/mastodon',
            'info' => 'https://fediverse.party/en/mastodon',
            'wizard' => '1',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/terms',
            'pp' => '/terms',
            'support' => '/about/more',
            'text' => 'mastodon',
            'href' => 'mastodon',
            'faclass' => 'fa-mastodon',
            'title' => 'Publish to mastodon'
        ],
        'hometown'    => [
            'repo' => 'hometown-fork/hometown',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'hometown-dev',
            'repository' => 'https://github.com/hometown-fork/hometown',
            'info' => 'https://fediverse.party/en/mastodon',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '/terms',
            'pp' => '/terms',
            'support' => '/about/more',
            'text' => 'hometown',
            'href' => 'hometown',
            'faclass' => 'fa-hometown',
            'title' => 'Publish to hometown'
        ],
        'pixelfed'     => [
            'repo' => 'pixelfed/pixelfed',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'dev',
            'repository' => 'https://github.com/pixelfed/pixelfed',
            'info' => 'https://fediverse.party/en/pixelfed',
            'wizard' => '0',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/site/terms',
            'pp' => '/site/privacy',
            'support' => '/site/contact',
            'text' => 'pixelfed',
            'href' => 'pixelfed',
            'faclass' => 'fa-pixelfed',
            'title' => 'Publish to pixelfed'
        ],
        'wordpress'    => [
            'repo' => 'wordpress/wordpress',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/Wordpress/Wordpress',
            'info' => 'https://wordpress.org',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '1',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'wordpress',
            'href' => 'wordpress',
            'faclass' => 'fa-wordpress',
            'title' => 'Publish to wordpress'
        ],
        'misskey'      => [
            'repo' => 'misskey-dev/misskey',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'develop',
            'repository' => 'https://github.com/misskey-dev/misskey',
            'info' => 'https://fediverse.party/en/misskey',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'privacy' => '',
            'support' => '',
            'text' => 'misskey',
            'href' => 'misskey',
            'faclass' => 'fa-misskey',
            'title' => 'Publish to misskey'
        ],
        'speechmore'   => [
            'repo' => '',
            'gitsite' => '',
            'gittype' => '',
            'devbranch' => '',
            'repository' => '',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'speechmore',
            'href' => 'speechmore',
            'faclass' => 'fa-speechmore',
            'title' => 'Publish to speechmore'
        ],
        'peertube'     => [
            'repo' => 'Chocobozzz/PeerTube',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'develop',
            'repository' => 'https://github.com/Chocobozzz/PeerTube',
            'info' => 'https://fediverse.party/en/peertube',
            'wizard' => '0',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/about/instance',
            'pp' => '/about/instance',
            'support' => '/about/instance',
            'text' => 'peertube',
            'href' => 'peertube',
            'faclass' => 'fa-peertube',
            'title' => 'Publish to peertube'
        ],
        'plume'        => [
            'repo' => 'Plume-org/Plume',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/Plume-org/Plume',
            'info' => 'https://joinplu.me/',
            'wizard' => '0',
            'welcome' => '1',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/about',
            'pp' => '/about',
            'support' => '/about',
            'text' => 'plume',
            'href' => 'plume',
            'faclass' => 'fa-plume plume',
            'title' => 'Publish to plume'
        ],
        'rustodon'     => [
            'repo' => 'rustodon/rustodon',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/rustodon/rustodon',
            'info' => '',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'rustodon',
            'href' => 'rustodon',
            'faclass' => 'fa-rustodon',
            'title' => 'Publish to rustodon'
        ],
        'microblogpub' => [
            'repo' => 'tsileo/microblog.pub',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => 'v2',
            'repository' => 'https://github.com/tsileo/microblog.pub',
            'info' => 'https://github.com/tsileo/microblog.pub',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'microblogpub',
            'href' => 'microblogpub',
            'faclass' => 'fa-microblogpub',
            'title' => 'Publish to microblogpub'
        ],
        'mobilizon'    => [
            'repo' => 'framasoft%2fmobilizon',
            'gitsite' => 'framagit.org',
            'gittype' => 'gitlab',
            'devbranch' => '',
            'repository' => 'https://framagit.org/framasoft/mobilizon',
            'info' => 'https://joinmobilizon.org/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '/terms',
            'pp' => '/privacy',
            'support' => '/about/instance',
            'text' => 'mobilizon',
            'href' => 'mobilizon',
            'faclass' => 'fa-mobilizon',
            'title' => 'Publish to mobilizon'
        ],
        'lemmy'        => [
            'repo' => 'LemmyNet/lemmy',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/fLemmyNet/lemmy',
            'info' => 'https://join-lemmy.org/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'lemmy',
            'href' => 'lemmy',
            'faclass' => 'fa-lemmy',
            'title' => 'Publish to lemmy'
        ],
        'gnusocial'        => [
            'repo' => 'GNUsocial/gnu-social',
            'gitsite' => 'code.undefinedhackers.net',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://code.undefinedhackers.net/GNUsocial/gnu-social',
            'info' => 'https://fediverse.party/en/gnusocial',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'gnusocial',
            'href' => 'gnusocial',
            'faclass' => 'fa-gnusocial',
            'title' => 'Publish to gnusocial'
        ],
        'ecko'        => [
            'repo' => 'magicstone-dev/ecko',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/magicstone-dev/ecko',
            'info' => 'https://magicstone.dev/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'ecko',
            'href' => 'ecko',
            'faclass' => 'fa-mastodon',
            'title' => 'Publish to ecko'
        ],
        'bookwyrm'        => [
            'repo' => 'bookwyrm-social/bookwyrm',
            'gitsite' => 'api.github.com',
            'gittype' => 'github',
            'devbranch' => '',
            'repository' => 'https://github.com/bookwyrm-social/bookwyrm',
            'info' => 'https://joinbookwyrm.com/',
            'wizard' => '0',
            'welcome' => '0',
            'protocol' => '0',
            'service' => '0',
            'terms' => '',
            'pp' => '',
            'support' => '',
            'text' => 'bookwyrm',
            'href' => 'bookwyrm',
            'faclass' => 'fa-bookwyrm',
            'title' => 'Publish to bookwyrm'
        ],
        'twitter'        => [
            'service' => '1',
            'text' => 'twitter',
            'faclass' => 'fa-twitter',
            'title' => 'Publish to twitter'
        ],
        'facebook'        => [
            'service' => '1',
            'text' => 'facebook',
            'faclass' => 'fa-facebook',
            'title' => 'Publish to facebook'
        ],
        'tumblr'        => [
            'service' => '1',
            'text' => 'tumblr',
            'faclass' => 'fa-tumblr',
            'title' => 'Publish to tumblr'
        ],
        'atom1.0'        => [
            'service' => '1',
            'text' => 'atom1.0',
            'faclass' => 'fa-rss-square',
            'title' => 'Publish to atom1.0'
        ],
        'rss2.0'        => [
            'service' => '1',
            'text' => 'rss2.0',
            'faclass' => 'fa-rss',
            'title' => 'Publish to rss2.0'
        ],
        'blogger'        => [
            'service' => '1',
            'text' => 'blogger',
            'faclass' => 'fa-bold',
            'title' => 'Publish to blogger'
        ],
        'google'        => [
            'service' => '1',
            'text' => 'google',
            'faclass' => 'fa-google-plus',
            'title' => 'Publish to google'
        ],
        'medium'        => [
            'service' => '1',
            'text' => 'medium',
            'faclass' => 'fa-medium',
            'title' => 'Publish to medium'
        ],
        'linkedin'        => [
            'service' => '1',
            'text' => 'linkedin',
            'faclass' => 'fa-linkedin',
            'title' => 'Publish to linkedin'
        ],
        'livejournal'        => [
            'service' => '1',
            'text' => 'livejournal',
            'faclass' => 'fa-book',
            'title' => 'Publish to livejournal'
        ],
        'pinterest'        => [
            'service' => '1',
            'text' => 'pinterest',
            'faclass' => 'fa-pinterest',
            'title' => 'Publish to pinterest'
        ],
        'pumpio'        => [
            'service' => '1',
            'text' => 'pumpio',
            'faclass' => 'fa-chevron-circle-right',
            'title' => 'Publish to pumpio'
        ],
        'ostatus'        => [
            'protocol' => '1',
            'service' => '0',
            'description' => '',
            'text' => 'ostatus',
            'faclass' => 'fa-genderless',
            'title' => 'ostatus'
        ],
        'activitypub'        => [
            'protocol' => '1',
            'service' => '0',
            'description' => '',
            'text' => 'activitypub',
            'faclass' => 'fa-activitypub',
            'title' => 'activitypub'
        ],
        'zot'        => [
            'protocol' => '1',
            'service' => '0',
            'description' => '',
            'text' => 'zot',
            'faclass' => 'fa-hubzilla',
            'title' => 'zot'
        ],
        'dfrn'        => [
            'protocol' => '1',
            'service' => '0',
            'description' => '',
            'text' => 'dfrn',
            'faclass' => 'fa-friendica',
            'title' => 'dfrn'
        ],
        'matrix'        => [
            'protocol' => '1',
            'service' => '0',
            'description' => '',
            'text' => 'matrix',
            'faclass' => 'fa-matrix-org',
            'title' => 'matrix'
        ],
        'webmention'        => [
            'protocol' => '1',
            'service' => '0',
            'description' => '',
            'text' => 'webmention',
            'faclass' => 'fa-wikipedia-w',
            'title' => 'webmention'
        ],
        'smtp'        => [
            'protocol' => '1',
            'service' => '1',
            'description' => '',
            'text' => 'smtp',
            'faclass' => 'fa-envelope-o',
            'title' => 'smtp'
        ],
        'xmpp'        => [
            'protocol' => '1',
            'service' => '1',
            'description' => '',
            'text' => 'xmpp',
            'faclass' => 'fa-xmpp',
            'title' => 'xmpp'
        ],
        ],
];
