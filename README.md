# Poduptime

Poduptime is software to get live stats and data on servers that support nodeinfo output.

# Translation Help
We can always use your help with languages
https://translate.diasp.org/

# To Install:

Environmental items you need (debian based system assumed):

OS Dependencies PHP8.2 & Postgresql 15:
```
php8.2-common php8.2 php8.2-curl php8.2-pgsql php8.2-zip php8.2-cli php8.2-fpm php8.2-bcmath php8.2-readline php8.2-mbstring php8.2-xml php8.2-intl git curl postgresql-15 postgresql-contrib postgresql-15-postgis dnsutils npm nodejs gzip
```

Yarn is a separate install: sudo npm install -g yarn  
Composer is a separate install: https://getcomposer.org/  
GeoIP database setup: https://github.com/maxmind/geoipupdate  

Clone and setup:
```
git clone https://gitlab.com/diasporg/poduptime
cd poduptime
yarn install
composer install
cp .env.example .env (all fields required)
```
Edit `.env` to update your DB and file settings

Import database schema to your postgresql database
```
psql database < database/tables.sql
```

# To Use:
***Backend***  
Main script:  
run `php app/helpers/update-all.php` to update your data for all servers  

Helper scripts (probably cronjobs):  
run `php app/helpers/update-remote-data.php` to pull in other json data as setup in config.php  
run `php app/helpers/update-static-files.php` to update rss feeds, sitemap, git version  
run `php app/helpers/update-monthly-stats.php` to gather stats update for /stats  
run `php app/helpers/database.php` to back up and clean/optimize the database  
Other scripts:  
run `php app/helpers/release-update-lock.php` to unlock an update-all.php run that died out  
run `php app/helpers/push-updates.php` to push to an api for updates  

***Frontend***  
Point your favorite websever to index.php  
Be sure to protect your .files, logs, backups etc. Using proper deny rules. https://serverfault.com/a/849537

# To Upgrade:
```
git pull
yarn install
composer install
psql database < database/migrations/xxx.sql (see database/README.md for proper migration versions)
```

# Status

[![pipeline status](https://gitlab.com/diasporg/poduptime/badges/develop/pipeline.svg)](https://gitlab.com/diasporg/poduptime/-/commits/develop)
[![coverage report](https://gitlab.com/diasporg/poduptime/badges/develop/coverage.svg)](https://gitlab.com/diasporg/poduptime/-/commits/develop)
[![Crowdin](https://badges.crowdin.net/poduptime/localized.svg)](https://crowdin.com/project/poduptime)

============================

Source for Poduptime

  Poduptime is software to get live stats and data on federated network servers.
  Copyright (C) 2011 David Morley

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
